package main

import (
    "fmt"
)

var (
    Version  string
    Build    string
)

func main() {
    fmt.Printf("Version: %s\n", Version)
    fmt.Printf("Build:   %s\n", Build)
}
