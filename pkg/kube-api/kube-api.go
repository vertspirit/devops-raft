package kube_api

import (
    "context"
    "log"
    "fmt"
    "time"
    "reflect"

    "k8s.io/client-go/kubernetes"
    "k8s.io/client-go/rest"
    "k8s.io/client-go/tools/clientcmd"
    "k8s.io/client-go/tools/clientcmd/api"
)

type KubeClient struct {
    ClientSet    *kubernetes.Clientset
    TokenAuth    *KubeTokenAuth
    MasterUrl    string                 `yaml:"master_url"`
    ClusterName  string                 `yaml:"cluster_name"`
    Namespace    string                 `yaml:"namespace"`
    ConfigFile   string                 `yaml:"config_file"`
    ConfigData   []byte
    Timeout      int64
}

func (kc *KubeClient) SetupClientSetByKubeConfig() error {
    var (
        config *rest.Config
        err    error
    )
    loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
    configOverrides := &clientcmd.ConfigOverrides{
        ClusterInfo: clientcmd.api.Cluster{Server: ""},
    }
    kconf := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
        loadingRules,
        configOverrides,
    ) // => clientcmd.ClientConfig
    // https://pkg.go.dev/k8s.io/client-go@latest/rest#Config
    // config, err := clientcmd.BuildConfigFromFlags(masterUrl, kubeconfigPath) => *rest.Config
    if kc.ConfigData != nil && len(kc.ConfigData) > 0 {
        config, err = kconf.ClientConfig() // => *rest.Config
        if err != nil {
            log.Printf("Fail to read config from the KUBECONFIG: %v\n", err)
            return err
        }
    } else {

    }
    kc.ClientSet, err = kubernetes.NewForConfig(config)
    if err != nil {
        log.Printf("Fail to create client with config from the KUBECONFIG: %v\n", err)
        return err
    }
    kc.Namespace, _, err = kc.ClientSet.Namespace()
    if err != nil {
        log.Printf("Fail to read namespace from the KUBECONFIG: %v\n", err)
        return err
    }
    return nil
}

func (kc *KubeClient) ListNamespaces(ctx *context.Context) ([]string, error) {
    var list []string
    // https://pkg.go.dev/k8s.io/apimachinery/pkg/apis/meta/v1#ListOptions
    opts := &kubernetes.metav1.ListOptions {
        TimeoutSeconds: kc.Timeout,
    }
    ns, err := kc.ClientSet.CoreV1().Namespaces().List(ctx, opts) // *kubernetes.corev1.NamespaceList
    if err != nil {
        log.Printf("List Namespaces Error: %v\n", err)
        return nil, err
    }
    for _, n := range ns.Items {
        list = append(list, n.Name)
    }
    return list, nil
}

func (kc *KubeClient) ListPods(ctx context.Context, ns string) ([]string, error) {
    var list []string
    if ns == "" {
        ns = kc.Namespace
    }
    pods, err := kc.ClientSet.CoreV1(ns).Pods().List(ctx, kubernetes.metav1.ListOptions{}) // *kubernetes.corev1.PodList
    if err != nil {
        log.Printf("List Pods Error: %v\n", err)
        return nil, err
    }
    for _, pd := range pods {
        list = append(list, pd.Name)
    }
    return list, nil
}

func (kc *KubeClient) GetPod(ctx context.Context, ns, pod string) (*kubernetes.corev1.Pod, error) {
    var pd *kubernetes.corev1.Pod
    if ns == "" {
        ns = kc.Namespace
    }
    pods, err := kc.ClientSet.CoreV1(ns).Pods().List(ctx, pod, kubernetes.metav1.ListOptions{}) // *kubernetes.corev1.PodList
    if err != nil {
        log.Printf("List Pods Error: %v\n", err)
        return nil, err
    }
    return pods.Items[0], nil
}

func (kc *KubeClient) ListServices(ctx *context.Context, ns ...string) ([]string, error) {
    var (
        list []string
        err  error
    )
    if len(ns) == 0 || ns[0] == "all" {
        ns, err = kc.ListNamespaces(ctx)
        if err != nil {
            return nil, err
        }
    } else {
        opts := kubernetes.metav1.ListOptions {
            TimeoutSeconds: kc.Timeout,
        }
        svcList, err := kc.ClientSet.AppsV1().Services(ns).List(ctx, opts) // *kubernetes.apiv1.ServiceList
        if err != nil {
            log.Printf("List Services Error: %v\n", err)
            return nil, err
        }
        fmt.Println("apiv1.ServiceList >>>", reflect.TypeOf(svcList))
        for _, sv := range svcList.Items {
            list = append(list, sv.Name)
        }
    }
    return list, nil
}

func (kc *KubeClient) ListDeployments(ctx *context.Context, ns string) ([]string, error) {
    var list []string
    deployments, err := kc.ClientSet.AppsV1().Deployments(ns).List(ctx, kubernetes.metav1.ListOptions{}) // *kubernetes.apiv1.DeploymentList
    if err != nil {
        log.Printf("List Deployments Error: %v\n", err)
        return nil , err
    }
    for _, deploy := range deployments {
        list = append(list, deploy.metav1.ObjectMeta.Name)
    }
    return list, nil
}

func (kc *KubeClient) CreateDeployment(ctx *context.Context, ns string)

type KubeTokenAuth struct {
    Host         string
    ApiUri       string
    User         string
    BearerToken  string
    CaFile       string
    CaData       []byte
    Timeout      int64
    Insecure     bool
}

func (kta *KubeTokenAuth) CreateClientByToken() (*kubernetes.Clientset, error) {
    config := &rest.Config {
        Host:         kta.Host,
        APIPath:      kta.ApiUri,
        Username:     kta.User,
        BearerToken:  kta.BearerToken,
        Timeout:      time.Duration(kta.Timeout) * time.Second,
        // Transport  http.RoundTripper
    }
    tc := &rest.TLSClientConfig {
        Insecure: kta.Insecure,
    }
    if kta.CaData != nil && len(kta.CaData) > 0 {
        tc.CAData = kta.CaData
    } else {
        tc.CAFile = kta.CaFile
    }
    config.TLSClientConfig = tc

    cs, err := kubernetes.NewForConfig(config)
    if err != nil {
        log.Printf("New Kube Client Error: %v\n", err)
        return nil, err
    }
    return cs, nil
}
