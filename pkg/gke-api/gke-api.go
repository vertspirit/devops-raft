package gke_api

import (
    "context"
    "encoding/base64"
    "fmt"
    "io/ioutil"
    "log"
    "reflect"

    "golang.org/x/oauth2/google"
    "google.golang.org/api/compute/v1"
    "google.golang.org/api/container/v1"
    "google.golang.org/api/option"
    "google.golang.org/api/transport"

    "k8s.io/client-go/kubernetes"
    _ "k8s.io/client-go/plugin/pkg/client/auth/gcp" // register GCP auth provider
    "k8s.io/client-go/tools/clientcmd"
    "k8s.io/client-go/tools/clientcmd/api"

    kapi "kube-client/pkg/kube-api"
)

type GcpKeyJson struct {
    KubeClient  *kapi.KubeClient
    ProjectId   string            `yaml:"project_id"`
    Scope       string            `yaml:"scopse"`
    KubeConfig  string            `yaml:"kube_config"`
    JsonFile    string            `yaml:"json_file"`
    JsonData    []byte
    Timeout     int64
}

func (gkj *GcpKeyJson) GetCredential(ctx *context.Context) *google.Credentials {
    data, err := ioutil.ReadFile(gkj.JsonFile)
    if err != nil {
        log.Fatalf("Fail to read key json file: %v\n", err)
    }
    creds, err := google.CredentialsFromJSON(ctx, data, gkj.Scope)
    if err != nil {
        log.Fatalf("Fail to create the google credential by key json file: %v\n", err)
    }
    // creds.ProjectID, creds.JSON
    return creds
}

func (gkj *GcpKeyJson) GetToken(ctx *context.Context) []byte {
    creds, err := transport.Creds(
        ctx,
        option.WithCredentialsFile(gkj.JsonFile),
        option.WithScopes(gkj.Scope),
    )
    token, err := creds.TokenSource.Token()
    if err != nil {
        log.Printf("Gett OAuth2 Token Error: %v\n", err)
        return make([]byte, 0)
    }
    return token
}

func (gkj *GcpKeyJson) GetApiAuth(token []byte) *api.AuthInfo {
    auth := &api.AuthInfo {
        Token:        token,
        AuthProvider: &api.AuthProviderConfig {
            Name: "gcp",
            Config: map[string]string {
                "scopes": gkj.Scope,
            },
        },
    }
    return auth
}

func (gkj *GcpKeyJson) ListGkeClusters(ctx *context.Context, zone string) ([]*container.Cluster, error) {
    var list []*container.Cluster
    svc, err := container.NewService(ctx, option.WithCredentialsFile(gkj.JsonFile))
    if err != nil {
        log.Printf("New Container Service Error: %v\n", err)
        return nil, err
    }
    req := svc.Projects.Zones.Clusters.List(gkj.ProjectId, zone)
    // svc.Projects.Zones.Clusters.List(gkj.ProjectId, zone).Context(ctx).Do() => only one page
    c, cancel := context.WithCancel(context.TODO())
    if err := req.Pages(c, func(page *container.ListClustersResponse) error {
        for _, cl := range page.Clusters {
            list = append(list, cl)
        }
        return nil
    }); err != nil {
        cancel()
        log.Printf("Get List From Pages Error: %v\n", err)
        return nil, err
    }
    cancel()
    return list, nil
}

func (gkj *GcpKeyJson) ListGkeClusterId(ctx *context.Context, zone string) (map[string]string, error) {
    var ids map[string]string
    list, err := gkj.ListGkeClusters(ctx, zone)
    if err != nil {
        return nil, err
    }
    for _, cluster := range list {
        ids[cluster.Name] = cluster.Id
    }
    return ids, nil
}

func (gkj *GcpKeyJson) GetGkeClusterId(ctx *context.Context, name, zone string) (string, error) {
    list, err := gkj.ListGkeClusterId(ctx, zone)
    if err != nil {
        return "", err
    }
    for k, v := range list {
        if k == name {
            return v, nil
        }
    }
    return "", fmt.Errorf("GKE cluster %s not found.\n", name)
}

func (gkj *GcpKeyJson) ListGkeClusterKubeConfig(ctx *context.Context, zone string) ([]*api.Config, error) {
    var confs  []*api.Config
    list, err := gkj.ListGkeClusters(ctx, zone)
    if err != nil {
        return nil, err
    }
    // for kubernetes config
    cnf := api.Config {
        APIVersion: "v1",
        Kind:       "Config",
        Clusters:   map[string]*api.Cluster{},
        Contexts:   map[string]*api.Context{},
        AuthInfos:  map[string]*api.AuthInfo{},
    }
    for _, cluster := range list {
        clusterName := fmt.Sprintf("gke-%s-%s-%s", gkj.ProjectId, cluster.Zone, cluster.Name)
        caCert, err := base64.StdEncoding.DecodeString(cluster.MasterAuth.ClusterCaCertificate)
        if err != nil {
            log.Printf("Get CA Certificate Of Cluster Error: %v\n", err)
            return nil, err
        }
        cnf.Contexts[clusterName] = &api.Context {
            Cluster:  clusterName,
            AuthInfo: clusterName,
        }
        cnf.Cluster[clusterName] = &api.Cluster {
            Server:                   fmt.Sprintf("https://%s", cluster.Endpoint),
            CertificateAuthorityData: caCert,
        }
        cnf.AuthInfos[clusterName] = &api.AuthInfo {
            AuthProvider: &api.AuthProviderConfig {
                Name:    "gcp",
                Config:  map[string]string {
                    // container.CloudPlatformScope
                    "scopes": gkj.Scope,
                },
            },
        }
        confs = append(confs, cnf)
    }
    return confs, nil
}

func (gkj *GcpKeyJson) GetGkeClusterConfig(ctx *context.Context, zone, id string) (*api.Config, error) {
    // GKE Cluster Name must be unique within this project and location
    svc, err := container.NewService(ctx, option.WithCredentialsFile(gkj.JsonFile))
    if err != nil {
        log.Printf("New Container Service Error: %v\n", err)
        return nil, err
    }
    res, err := svc.Projects.Zones.Clusters.Get(gkj.ProjectId, zone, id).Context(ctx).Do()
    if err != nil {
        log.Printf("Get GKE Cluster Error: %v\n", err)
        return nil, err
    }
    clusterName := fmt.Sprintf("gke-%s-%s-%s", gkj.ProjectId, res.Zone, res.Name)
    cert, err := base64.StdEncoding.DecodeString(res.MasterAuth.ClusterCaCertificate)
    if err != nil {
        log.Printf("Get CA Certificate Of Cluster Error: %v\n", err)
        return nil, err
    }
    conf := api.Config {
        APIVersion: "v1",
        Kind:       "Config",
        Clusters:   map[string]*api.Cluster{},
        Contexts:   map[string]*api.Context{},
        AuthInfos:  map[string]*api.AuthInfo{},
    }
    conf.Contexts[clusterName] = &api.Context {
        Cluster:  clusterName,
        AuthInfo: clusterName,
    }
    conf.Cluster[clusterName] = &api.Cluster {
        Server:                   fmt.Sprintf("https://%s", res.Endpoint),
        CertificateAuthorityData: cert,
    }
    conf.AuthInfos[clusterName] = &api.AuthInfo {
        AuthProvider: &api.AuthProviderConfig {
            Name:    "gcp",
            Config:  map[string]string {
                "scopes": gkj.Scope, // container.CloudPlatformScope
            },
        },
    }
    return conf, nil
}

func (gkj *GcpKeyJson) SetupKubeClient(ctx *context.Context, name, zone string) error {
    id, err := gkj.GetGkeClusterId(ctx, name, zone)
    if err != nil {
        log.Printf("Setup kubernetes Client Error: %v\n", err)
        return err
    }
    apiConfig, err := gkj.GetGkeClusterConfig(ctx, zone, id)
    if err != nil {
        log.Printf("Setup kubernetes Client Error: %v\n", err)
        return err
    }
    gkj.KubeClient, err = CreateKubeClient(apiConfig, gkj.Timeout)
    if err != nil {
        log.Printf("Setup kubernetes Client Error: %v\n", err)
        return err
    }
    return nil
}

func (gkj *GcpKeyJson) WriteToKubeConfigFile(ctx *context.Context, conf *api.Config) {
    clientcmd.WriteToFile(conf,  gkj.KubeConfig)
}

func CreateKubernetesClientFromConfigFile(file string, timeout int64) *kubernetes.Clientset {
    conf, err := ioutil.ReadFile(file)
    if err != nil {
        log.Printf("Fail to read kubeconfig file: %v\n", err)
        return nil
    }
    clientConf, err := clientcmd.NewClientConfigFromBytes(conf)
    if err != nil {
        log.Printf("Fail to create client config: %v\n", err)
        return nil
    }
    restConf, err := clientConf.ClientConfig()
    if err != nil {
        log.Printf("Fail to create restful api config: %v\n", err)
        return nil
    }
    restConf.Timeout = timeout
    clientSet, err := kubernetes.NewForConfig(restConf)
    if err != nil {
        log.Printf("Fail to create client set: %v\n", err)
        return nil
    }
    return clientSet
}

func CreateKubeClient(conf *api.Config, timeout int64) (*kubernetes.Clientset, error) {
    clientConf := clientcmd.NewDefaultClientConfig(conf)
    restConf, err := clientConf.ClientConfig()
    if err != nil {
        log.Printf("Fail to create restful api config: %v\n", err)
        return nil, err
    }
    restConf.Timeout = timeout
    clientSet, err := kubernetes.NewForConfig(restConf)
    if err != nil {
        log.Printf("Fail to create client set: %v\n", err)
        return nil, err
    }
    return clientSet, nil
}

func CreateContainerService(ctx *context.Context, authType string, cred *google.Credentials,
    token []byte, json, apikey string) (*container.Service, error) {
    var svc *container.Service
    var err error
    switch authType {
    case "json":
        if json == "" {
            log.Println("The key json is invalid.")
            return nil, fmt.Errorf("The key json is invalid.")
        }
        svc, err = container.NewService(ctx, option.WithCredentialsFile(json))
        if err != nil {
            log.Printf("Fail to create google container service with the key json file: %v\n", err)
            return nil, err
        }
    case "cred":
        if cred == nil {
            log.Println("The credential auth is invalid.")
            return nil, fmt.Errorf("The credential auth is invalid.")
        }
        svc, err = container.NewService(ctx, option.WithCredentials(cred))
        if err != nil {
            log.Printf("Fail to create google container service with the credential auth: %v\n", err)
            return nil, err
        }
    case "token":
        if len(token) == 0 {
            log.Println("The token is empty.")
            return nil, fmt.Errorf("The token is empty.")
        }
        svc, err = container.NewService(ctx, option.WithTokenSource(cred.TokenSource(ctx, token)))
        if err != nil {
            log.Printf("Fail to create google container service with the auth token: %v\n", err)
            return nil, err
        }
    case "apikey":
        if apikey == "" {
            log.Println("The api key is invalid.")
            return nil, fmt.Errorf("The api key is invalid.")
        }
        svc, err = container.NewService(ctx, option.WithAPIKey(apikey))
        if err != nil {
            log.Printf("Fail to create google container service with the api key: %v\n", err)
            return nil, err
        }
    }
    return svc, nil
}
// only gce: compute.ComputeScope
