package worker_pool

import (
    "context"
    "sync"
)

type WorkerPool interface {
    Allocate(ctx context.Context, profile *QueuedJobProfile)
    Consume(ctx context.Context)
    Read(ctx context.Context, data *[]interface{})
    Wait(ctx context.Context)
}

type generalPool struct {
    Wg       sync.WaitGroup
    Queued   chan QueuedJob
    Results  chan interface{}
    Done     chan bool
    Works    int
}

type QueuedJob interface {
    Run(ctx context.Context) (interface{}, error)
}

type Task func(ctx context.Context, data interface{}, params ...interface{}) (interface{}, error)

type QueuedJobProfile struct {
    Task    Task
    Inputs  []interface{}
    Params  []interface{}
}

type Job struct {
    Task  Task
    Data  interface{}
    Args  []interface{}
}

func (jc  *Job) Run(ctx context.Context) (interface{}, error) {
    out, err := jc.Task(ctx, jc.Data, jc.Args...)
    if err != nil {
        return nil, err
    }
    return out, nil
}

func NewWorkerPool(num int) WorkerPool {
    wp := &generalPool {
        Wg:       sync.WaitGroup{},
        Works:    num,
        Queued:   make(chan QueuedJob, num),
        Results:  make(chan interface{}, num),
        Done:     make(chan bool),
    }
    return wp
}

func (gp *generalPool) Allocate(ctx context.Context, profile *QueuedJobProfile) {
    for _, in := range profile.Inputs {
        jb := &Job {
            Task: profile.Task,
            Data: in,
            Args: profile.Params,
        }
        gp.Queued <- jb
    }
    close(gp.Queued)
}

func (gp *generalPool) Consume(ctx context.Context) {
    for n := 1; n <= gp.Works; n++ {
        gp.Wg.Add(1)
        go func() {
            defer gp.Wg.Done()
            for job := range gp.Queued {
                if out, err := job.Run(ctx); err == nil {
                    gp.Results <-out
                }
            }
        }()
    }
    gp.Wg.Wait()
    close(gp.Results)
}

func (gp *generalPool) Read(ctx context.Context, data *[]interface{}) {
    for output := range gp.Results {
        *data = append(*data, output)
    }
    gp.Done <- true
}

func (gp *generalPool) Wait(ctx context.Context) {
    <-gp.Done
    close(gp.Done)
}

func IfsToStrings(inputs []interface{}) []string {
    var list []string
    for _, in := range inputs {
        switch in.(type) {
        case string:
            list = append(list, in.(string))
        }
    }
    return list
}

func StringsToIfs(inputs []string) []interface{} {
    var list []interface{}
    for _, in := range inputs {
        list = append(list, in)
    }
    return list
}

