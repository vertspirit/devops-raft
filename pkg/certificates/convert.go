package certificates

import (
    "encoding/pem"
    "crypto/x509"
    "golang.org/x/crypto/pkcs12"
    "crypto/rand"
    "os"
    "log"

    gopkcs12 "software.sslmate.com/src/go-pkcs12"
)

type PemCertificateFile struct {
    PrivkeyFile  string
    CertFile     string
    CaFile       string
    OutputFile   string
    Password     string
    PemPassword  string
}

type PfxCertificateFile struct {
    InputFile     string
    PrivkeyFile   string
    CertFile      string
    Password      string
    EraseHeaders  bool
}

func (pemc *PemCertificateFile) RemovePemPasword(file string) {
    crtfile, err := os.ReadFile(pemc.CertFile)
    if err != nil {
        log.Fatalf("Fail to load the certificate pem file: %v\n", err)
    }
    block, _ := pem.Decode(crtfile)
    if block == nil || block.Type != "PUBLIC KEY" {
        log.Fatal("Fail to decode certificate pem block.")
    }
    if x509.IsEncryptedPEMBlock(block) {
        log.Println("Deprecated: Legacy PEM encryption as specified in RFC 1423 is insecure by design.Since it does not authenticate the ciphertext, it is vulnerable to padding oracle attacks that can let an attacker recover the plaintext.")
        der, err := x509.DecryptPEMBlock(block, []byte(pemc.PemPassword))
        if err != nil {
            log.Fatalf("Fail to decrypted pem block: %v", err)
        }
        decrypted, err := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_APPEND|os.O_TRUNC, 0600)
        if err != nil {
            log.Fatalf("Fail to open a file to write decrypted pem into: %v", err)
        }
        defer decrypted.Close()
        decrypted.Write(der)
    }
}

func (pemc *PemCertificateFile) ConvertPemToPfx() {
    var caPool []*x509.Certificate

    // os.ReadFile() required go v1.16 or above
    crtfile, err := os.ReadFile(pemc.CertFile)
    if err != nil {
        log.Fatalf("Fail to load the certificate pem file: %v\n", err)
    }
    keyfile, err := os.ReadFile(pemc.PrivkeyFile)
    if err != nil {
        log.Fatalf("Fail to load the private key pem file: %v\n", err)
    }
    cafile, err := os.ReadFile(pemc.CaFile)
    if err != nil {
        log.Fatalf("Fail to load the ca certificate pem file: %v\n", err)
    }

    crtBlock, rest := pem.Decode(crtfile)
    for len(rest) != 0 {
        crtBlock, rest = pem.Decode(rest)
        if crtBlock == nil {
            log.Fatal("Fail to decode certificate pem block.")
        }
    }
    keyBlock, _ := pem.Decode(keyfile)
    if keyBlock == nil {
        log.Fatal("Fail to decode private key pem block.")
    }
    caBlock, _ := pem.Decode(cafile)
    if keyBlock == nil {
        log.Fatal("Fail to decode ca certificate pem block.")
    }

    crt, err := x509.ParseCertificate(crtBlock.Bytes)
    if err != nil {
        log.Fatalf("Fail to pasrse the certificate: %v\n", err)
    }
    key, err := x509.ParsePKCS1PrivateKey(keyBlock.Bytes)
    if err != nil {
        log.Fatalf("Fail to pasrse the private key: %v\n", err)
    }
    ca, err := x509.ParseCertificate(caBlock.Bytes)
    if err != nil {
        log.Fatalf("Fail to pasrse the ca certifcate: %v\n", err)
    }
    if ca.IsCA {
        caPool = append(caPool, ca)
    } else {
        log.Println("This is not a ca certificate.")
    }

    pfx, err := gopkcs12.Encode(rand.Reader, key, crt, caPool, pemc.Password)
    if err != nil {
        log.Fatalf("Fail to convert pem to pfx: %v\n", err)
    }

    file, err := os.Create(pemc.OutputFile)
    if err != nil {
        log.Fatalf("Fail to create file: %v\n", err)
    }
    defer file.Close()

    numb, err := file.Write(pfx)
    if err != nil {
        log.Fatalf("Fail to write into a file: %v\n", err)
    } else {
        log.Printf("Wrote %d bytes to file.\n", numb)
    }
}

func (pfxc *PfxCertificateFile) RemovePfxPassword(file string) {
    loadFile, err := os.ReadFile(pfxc.InputFile)
    if err != nil {
        log.Fatalf("Fail to read pfx file: %v\n", err)
    }
    privkey, cert, certs, err := gopkcs12.DecodeChain(loadFile, pfxc.Password)
    if err != nil {
        log.Fatalf("Fail to decode the pfx: %v", err)
    }
    emptyPassword := ""
    pfx, err := gopkcs12.Encode(rand.Reader, privkey, cert, certs, emptyPassword)
    if err != nil {
        log.Fatalf("Fail to encode new pfx: %v", err)
    }
    newfile, err := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_APPEND|os.O_TRUNC, 0600)
    if err != nil {
        log.Fatalf("Fail to open a file to write pfx: %v", err)
    }
    defer newfile.Close()
    newfile.Write(pfx)
}

func (pfxc *PfxCertificateFile) ConvertPfxToPem() {
    loadFile, err := os.ReadFile(pfxc.InputFile)
    if err != nil {
        log.Fatalf("Fail to read file: %v\n", err)
    }
    blocks, err := pkcs12.ToPEM(loadFile, pfxc.Password)
    if err != nil {
        log.Fatalf("Fail to decode to pem from PKCS12: %v\n", err)
    }
    if len(blocks) < 2 {
        log.Fatal("Wrong password for pfx certificate.")
    }
    // remove all optional headers if enabled the parameter
    if pfxc.EraseHeaders {
        for n := 0; n < len(blocks); n++ {
            blocks[n].Headers = make(map[string]string)
        }
    }
    privKey, err := os.OpenFile(pfxc.PrivkeyFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND|os.O_TRUNC, 0600)
    if err != nil {
        log.Fatalf("Fail to open a file to write private key: %v", err)
    }
    defer privKey.Close()
    certs, err := os.OpenFile(pfxc.CertFile, os.O_WRONLY|os.O_CREATE|os.O_APPEND|os.O_TRUNC, 0600)
    if err != nil {
        log.Fatalf("Fail to open a file to write certificate: %v", err)
    }
    defer certs.Close()
    for n := 0; n < len(blocks); n++ {
        if n != (len(blocks) - 1) {
            err := pem.Encode(certs, blocks[n])
            if err != nil {
                log.Fatalf("Fail to encode certificate block to pem: %v\n", err)
            }
        } else {
            err := pem.Encode(privKey, blocks[n])
            if err != nil {
                log.Fatalf("Fail to encode private key block to pem: %v\n", err)
            }
        }
    }
}
