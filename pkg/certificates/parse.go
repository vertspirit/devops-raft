package certificates

import (
    "io/ioutil"
    "crypto/x509"
    "golang.org/x/crypto/pkcs12"
    "encoding/pem"
    "log"
    "strings"
    "time"
)

type Certificate struct {
    File      string
    Format    string   // pem or pfx
    Password  string
}

func (c *Certificate) LoadAndParseCertFile() *x509.Certificate {
    var crt *x509.Certificate

    cert, err := ioutil.ReadFile(c.File)
    if err != nil {
        log.Fatalf("Fail to read certificate file: %v\n", err)
    }

    switch strings.ToLower(c.Format) {
    case "pfx":
        _, crt, err = pkcs12.Decode(cert, c.Password)
        if err != nil {
            log.Fatalf("Fail to decode pfx file: %v\n", err)
        }
    default:
        // default format is pem
        crtBlock, _ := pem.Decode(cert)
        crt, err = x509.ParseCertificate(crtBlock.Bytes)
        if err != nil {
            log.Fatalf("Fail to parse this certificate: %v\n", err)
        }
    }
    return crt
}

func (c *Certificate) GetExpiration(crt *x509.Certificate) string {
    return crt.NotAfter.Format("2006-01-02T15:04:05-0700")
}

func (c *Certificate) GetExpiredNextDays(crt *x509.Certificate) int64 {
    expired := crt.NotAfter
    end := expired.UnixNano() / int64(time.Millisecond)
    start := time.Now().UnixNano() / int64(time.Millisecond)
    diff := end - start
    nextDays := diff / (1000 * 86400)
    return nextDays
}

func (c *Certificate) GetIssuer(crt *x509.Certificate) string {
    return crt.Issuer.Organization[0]
}

func (c *Certificate) IncludeCABundle(crt *x509.Certificate) bool {
    switch strings.ToLower(c.Format) {
    case "pfx":
        return false
    default:
        cert, err := ioutil.ReadFile(c.File)
        if err != nil {
            log.Fatalf("Fail to read certificate file: %v\n", err)
        }
        _, restBlock := pem.Decode(cert)
        if restBlock == nil {
            return false
        } else {
            return true
        }
    }
}

func (c *Certificate) VertifyPemCertChain(crt *x509.Certificate) bool {
    rootCas := x509.NewCertPool()
    intermediateCerts := x509.NewCertPool()

    certs, err := ioutil.ReadFile(c.File)
    if err != nil {
        log.Fatalf("Fail to read certificate file: %v\n", err)
    }

    switch strings.ToLower(c.Format) {
    case "pfx":
        log.Printf("The verify-cert-chain function only support pem format certificate.")
        return false
    default:
        crtBlk, restBlk := pem.Decode(certs)
        crt, err = x509.ParseCertificate(crtBlk.Bytes)
        if err != nil {
            log.Printf("Fail to parse certificate: %v\n", err)
            return false
        }
        certPool, err := x509.ParseCertificates(restBlk)
        if err != nil {
            log.Printf("Fail to parse certificates: %v\n", err)
            return false
        }
        if len(certPool) == 0 {
            return false
        }
        for n := 0; n < len(certPool); n++ {
            if certPool[n].IsCA {
                rootCas.AddCert(certPool[n])
            } else {
                intermediateCerts.AddCert(certPool[n])
            }
        }
    }
    verifyOpt := x509.VerifyOptions {
        Roots:         rootCas,
        Intermediates: intermediateCerts,
        KeyUsages:     []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
    }
    _, err = crt.Verify(verifyOpt)
    if err != nil {
        log.Printf("Fail to verify the certificate with CA root: %v\n", err)
        return false
    }
    return true
}
