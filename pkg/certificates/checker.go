package certificates

import (
    "crypto/tls"
    "crypto/x509"
    "io/ioutil"
    "log"
    "strconv"
    "time"
)

type WebSite struct {
    Domain     string
    Port       int
    CAFile     string
    Insecure   bool
    MinTLSVer  string
}

func (w *WebSite) Connect() *tls.Conn {
    addr := w.Domain + ":" + strconv.Itoa(w.Port)
    conf := &tls.Config {
        InsecureSkipVerify: w.Insecure,
    }
    switch w.MinTLSVer {
    case "1.1":
        conf.MinVersion = tls.VersionTLS11
    case "1.2":
        conf.MinVersion = tls.VersionTLS12
    case "1.3":
        conf.MinVersion = tls.VersionTLS13
    default:
        conf.MinVersion = tls.VersionTLS12
    }
    // To print tls version with type uint16: VersionTLS11=770, VersionTLS12=771, VersionTLS13=772
    // log.Printf("Min TLS Version: %v\n", tls.VersionTLS11)
    if w.CAFile != "" {
        rootCA := x509.NewCertPool()
        caCert, err := ioutil.ReadFile(w.CAFile)
        if err != nil {
            log.Fatalf("Fail to load the CA root certitifcate: %v\n", err)
        }
        rootCA.AppendCertsFromPEM(caCert)
        conf.RootCAs = rootCA
    }
    conn, err := tls.Dial("tcp", addr, conf)
    if err != nil {
        log.Fatalf("Connect to %s failed: %v\n", addr, err)
    }
    return conn
}

func (w *WebSite) VerifyDomain(conn *tls.Conn, domain string) bool {
    err := conn.VerifyHostname(domain)
    if err != nil {
        log.Fatalf("Fail to verify %s in the website's certificate: %v\n", domain, err)
        return false
    }
    return true
}

func (w *WebSite) GetExpiration(conn *tls.Conn) string {
    expired := conn.ConnectionState().PeerCertificates[0].NotAfter
    return expired.Format("2006-01-02T15:04:05-0700") // time.RFC3339
}

func (w *WebSite) GetExpiredNextDays(conn *tls.Conn) int64 {
    expired := conn.ConnectionState().PeerCertificates[0].NotAfter
    end := expired.UnixNano() / int64(time.Millisecond)
    start := time.Now().UnixNano() / int64(time.Millisecond)
    diff := end - start
    nextDays := diff / (1000 * 86400)
    return nextDays
}

func (w *WebSite) GetIssuer(conn *tls.Conn) string {
    issuer := conn.ConnectionState().PeerCertificates[0].Issuer
    return issuer.Organization[0]
}

func (w *WebSite) GetOCSPResponse (conn *tls.Conn) string {
    resp := conn.ConnectionState().OCSPResponse
    if len(resp) > 0 {
        return string(resp)
    } else {
        return "Fail to get response or this certificate is not stapled OCSP."
    }
}

func (w *WebSite) VerifyFullChain(conn *tls.Conn) bool {
    certs := conn.ConnectionState().PeerCertificates
    intermediateCerts := x509.NewCertPool()
    rootCas := x509.NewCertPool()
    for n := 1; n < len(certs); n++ {
        if certs[n].IsCA {
            rootCas.AddCert(certs[n])
        } else {
            intermediateCerts.AddCert(certs[n])
        }
    }
    verifyOpt := x509.VerifyOptions {
        Roots:         rootCas,
        Intermediates: intermediateCerts,
        KeyUsages:     []x509.ExtKeyUsage{x509.ExtKeyUsageAny},
    }
    _, err := certs[0].Verify(verifyOpt)
    if err != nil {
        log.Printf("Fail to verify the certificate with CA root: %v\n", err)
        return false
    }
    return true
}
