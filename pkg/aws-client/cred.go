package aws_client

import (
    "log"
    "context"
    "os"
    "github.com/aws/aws-sdk-go-v2/aws"
    "github.com/aws/aws-sdk-go-v2/config"
    "github.com/aws/aws-sdk-go-v2/credentials"
)

type AwsConfig struct {
    AccessKeyId              string
    AccessSecretKey         string
    SessionToken             string // optional
    SharedCredentialsFile  string
    Region                    string
}

func (awsc *AwsConfig) CreateConfig(ctx context.Context) (aws.config, error) {
    /*
    Priority: arguments, env variable, custom credentails file, default credentails file
    Defualt Credentials File (~/.aws/credentials):
        [default]
        aws_access_key_id = <YOUR_ACCESS_KEY_ID>
        aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>
        aws_session_token = <YOUR_SESSION_TOKEN>
    Defualt Config File (~/.aws/credentials):
        [default]
        region = <REGION>
    */
    var (
        cfg aws.config
        err error
    )
    if awsc.AccessKeyId == "" {
        awsc.AccessKeyId  = os.Getenv("AWS_ACCESS_KEY_ID")
    }
    if awsc.AccessSecretKey == "" {
        awsc.AccessSecretKey = os.Getenv("AWS_SECRET_ACCESS_KEY")
    }
    if awsc.SessionToken == "" {
        awsc.SessionToken = os.Getenv("AWS_SESSION_TOKEN")
    }
    if awsc.Region == "" {
        awsc.Region = os.Getenv("AWS_DEFAULT_REGION")
    }
    if awsc.SharedCredentialsFile == "" {
        awsc.SharedCredentialsFile = os.Getenv("AWS_SHARED_CREDENTIALS_FILE")
    }
    if awsc.AccessKeyId != "" && awsc.AccessSecretKey != "" {
        cred := credentials.NewStaticCredentialsProvider(
            awsc.AccessKeyId,
            awsc.AccessSecretKey,
            awsc.SessionToken,
        )
        if cfg, err = config.LoadDefaultConfig(
            ctx,
            config.WithCredentialsProvider(cred),
            config.WithRegion(awsc.Region),
        ); err != nil {
            log.Printf("Create Config Error: %v\n", err)
            return nil, err
        }
    } else {
        if awsc.SharedCredentialsFile != "" {
            if cfg, err = config.LoadDefaultConfig(
                ctx,
                config.WithSharedCredentialsFiles([]string{ awsc.SharedCredentialsFile }),
            ); err != nil {
                log.Printf("Load Shared Credentials File Error: %v\n", err)
                return nil, err
            }
        } else {
            cfg, err = config.LoadDefaultConfig(ctx, config.WithRegion(awsc.Region))
            if err != nil {
                log.Printf("Load Default Config Error: %v\n", err)
                return nil, err
            }
        }
    }
    return cfg, nil
}
