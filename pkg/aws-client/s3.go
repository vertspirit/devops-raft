package aws_client

import (
    "bufio"
    "context"
    "errors"
    "fmt"
    "log"
    "os"
    "path/filepath"
    "regexp"
    "strings"
    "sync"
    "time"

    "github.com/aws/aws-sdk-go-v2/aws"
    "github.com/aws/aws-sdk-go-v2/service/s3"
    s3types "github.com/aws/aws-sdk-go-v2/service/s3/types"
    s3manager "github.com/aws/aws-sdk-go-v2/feature/s3/manager"
    "github.com/aws/smithy-go"

    "devops-essential/pkg/worker-pool"
)

type S3Client struct {
    Config        *AwsConfig
    Client        *s3.Client
    CoRoutines    int
    CoDownloads   int  // 3,500 PUT/COPY/POST/DELETE for aws s3 api
    CoUploads     int  // 5,500 GET/HEAD for aws s3 api
}

func (sc *S3Client) CreateClinent(ctx context.Context) error {
    cfg, err := sc.Config.CreateConfig(ctx)
    if err != nil {
        return err
    }
    sc.Client = s3.NewFromConfig(cfg)
    return nil
}

func (sc *S3Client) GetBuckets(ctx context.Context, region string) ([]s3types.Bucket, error) {
    var buckets  []s3types.Bucket
    list, err := sc.Client.ListBuckets(ctx, &s3.ListBucketsInput{})
    if err != nil {
        log.Printf("List Bucket Error: %v\n", err)
        return nil, err
    }
    switch region {
    case "", "global":
        return list.Buckets, nil
    default:
        wg := sync.WaitGroup{}
        ch := make(chan s3types.Bucket)
        for _, bk := range list.Buckets {
            wg.Add(1)
            go func(bk s3types.Bucket) {
                defer wg.Done()
                check, err := sc.CheckBucketLocation(ctx, bk, region)
                if check != nil && err == nil {
                    ch <- bk
                }
            }(bk)
        }
        go func() {
            wg.Wait()
            close(ch)
        }()
        for bucket := range ch {
            buckets = append(buckets, bucket)
        }
    }
    return buckets, nil
}

func (sc *S3Client) GetBucketsWithPool(ctx context.Context, region string) ([]s3types.Bucket, error) {
    var buckets  []s3types.Bucket
    list, err := sc.Client.ListBuckets(ctx, &s3.ListBucketsInput{})
    if err != nil {
        log.Printf("List Bucket Error: %v\n", err)
        return nil, err
    }
    switch region {
    case "", "global":
        return list.Buckets, nil
    default:
        wp := worker_pool.NewWorkerPool(sc.CoRoutines)
        args := make([]interface{}, 1)
        args[0] = region
        jobs := &worker_pool.QueuedJobProfile {
            Task:   sc.CheckBucketLocation,
            Inputs: list.Buckets,
            Params: args,
        }
        bkList := make([]interface{}, 0)
        go wp.Allocate(ctx, jobs)
        go wp.Read(ctx, &bkList)
        wp.Consume(ctx)
        wp.Wait(ctx)
        for _, bk := range bkList {
            buckets = append(buckets, bk.(s3types.Bucket))
        }
    }
    return buckets, nil
}

func (sc *S3Client) ListBuckets(ctx context.Context, region string, pool bool) ([]string, error) {
    var list []string
    if pool {
        buckets, err := sc.GetBucketsWithPool(ctx, region)
        if err != nil {
            return nil, err
        }
        for _, item := range buckets {
            list = append(list, *item.Name)
        }
    } else {
        buckets, err := sc.GetBuckets(ctx, region)
        if err != nil {
            return nil, err
        }
        for _, item := range buckets {
            list = append(list, *item.Name)
        }
    }
    return list, nil
}

func (sc *S3Client) CheckBucketLocation(ctx context.Context, data interface{}, args ...interface{}) (interface{}, error) {
    bucket := data.(s3types.Bucket)
    region := args[0].(string)
    loc, err := sc.Client.GetBucketLocation(
        ctx,
        &s3.GetBucketLocationInput {
            Bucket: bucket.Name,
        },
    )
    if err != nil {
        log.Printf("Get Location Error: %v\n", err)
        return nil, err
    }
    if region == fmt.Sprintf("%v", loc.LocationConstraint) {
        return bucket, nil
    }
    return nil, fmt.Errorf("Not matched.\n")
}

func (sc *S3Client) IsBucketExits(ctx context.Context, bucket string) (bool, error) {
    var ae smithy.APIError
    obj, err := sc.Client.HeadBucket(ctx, &s3.HeadBucketInput {
        Bucket: aws.String(bucket),
    })
    if errors.As(err, &ae) {
        switch ae.ErrorCode() {
        case "NoSuchBucket":
            return false, ae.ErrorMessage()
        default:
            return false, ae.ErrorFault().String()
        }
    }
    return true, nil
}

type ListObjectConfig struct {
    Bucket      string
    Prefix      string  // Only the object key which start with specific prefix string
    Delimiter   string  // exclude the object contains specifc characters
    MaxKeys     int     // keys per page to retrieve at once
}

func (sc *S3Client) ListObjects(ctx context.Context, opts *ListObjectConfig, before int64) ([]string, error) {
    /*
       The beforce parameter could be nagtive value to get objects modified in minutes.
    */
    input := &s3.ListObjectsV2Input {
        Bucket: aws.String(opts.Bucket),
    }
    if len(opts.Prefix) > 0 {
        input.Prefix = opts.Prefix
    }
    if len(opts.Delimiter) > 0 {
        input.Delimiter = opts.Delimiter
    }
    paginator, err := s3.NewListObjectsV2Paginator(
        sc.Client,
        input,
        func(opt *s3.ListObjectsV2PaginatorOptions) {
            if opts.MaxKeys != 0 {
                opt.Limit = opts.MaxKeys
            }
        },
    )
    list := make([]interface{}, 0)
    for paginator.HasMorePages() {
        page, err := paginator.NextPage(ctx)
        if err != nil {
            log.Printf("Object Page Error: %v\n", err)
            continue
        }
        for _, obj := range page.Contents {
            list = append(list, obj)
        }
    }
    keyList := make([]string, 0)
    if before != 0 {
        wp := worker_pool.NewWorkerPool(sc.CoRoutines)
        args := make([]interface{}, 1)
        args[0] = before
        jobs := &worker_pool.QueuedJobProfile {
            Task:    sc.CheckBucketLocation,
            Inputs:  list,
            Params:  args,
        }
        objList := make([]interface{}, 0)
        go wp.Allocate(ctx, jobs)
        go wp.Read(ctx, &objList)
        wp.Consume(ctx)
        wp.Wait(ctx)
        keyList = worker_pool.IfsToStrings(objList)
    } else {
        for _, obj := range list {
            keyList = append(keyList, *obj.(s3types.Object).Key)
        }
    }
    return keyList, nil
}

func checkObjectModified(ctx context.Context, data interface{}, params ...interface{}) (interface{}, error) {
    obj := data.(s3types.Object)
    before := float64(params[0].(int64))
    diff := time.Now().Sub(obj.LastModified).Minutes()
    switch {
    case before < 0:
        if diff > before {
            return *obj.Key, nil
        }
    case before > 0:
        if diff > before {
            return *obj.Key, nil
        }
    }
    return nil, fmt.Errorf("Not matched.\n")
}

func (sc *S3Client) GetObject(ctx context.Context, bucket, objKey string) (s3types.Object, error) {
    obj, err := sc.Client.GetObject(
        ctx,
        &s3.GetObjectInput {
            Bucket:  aws.String(bucket),
            Key:     aws.String(objKey),
        },
    )
    if err != nil {
        var ae smithy.APIError
        if errors.As(err, &ae) {
            switch {
            case ae.ErrorCode() == "NoSuchBucket":
                log.Printf("The bucket %s not exists.\n", bucket)
            case ae.ErrorCode() == "NoSuchKey":
                log.Printf("The object key %s not exists.\n", objKey)
            default:
                log.Printf("Error Code: %v\n", err)
            }
        }
        return nil, err
    }
    return obj, nil
}

func (sc *S3Client) IsObjectExits(ctx context.Context, bucket, objKey string) (bool, error) {
    var ae smithy.APIError
    /*
        The HEAD action retrieves metadata from an object without returning the object itself.
        This action is useful if you're only interested in an object's metadata.
        To use HEAD, you must have READ access to the object.
    */
    obj, err := sc.Client.HeadObject(ctx, &s3.HeadObjectInput {
        Bucket:  aws.String(bucket),
        Key:     aws.String(objKey),
    })
    if errors.As(err, &ae) {
        switch ae.ErrorCode() {
        case "NoSuchKey":
            return false, ae.ErrorMessage()
        default:
            return false, ae.ErrorFault().String()
        }
    }
    return true, nil
}

func ObjectsToDirectoriesAndFiles(ctx context.Context, objKeys []string) ([]string, []string, error) {
    var (
        dirLsit  []string
        fileList []string
    )
    re, err := regexp.Compile(`.*/$`)
    if err != nil {
        log.Printf("Compile Regular Expression Error: %v\n", err)
    }
    wg := sync.WaitGroup{}
    dirCh := make(chan string, 1024)
    fileCh := make(chan string, 1024)
    done := make(chan bool)
    rwmu := sync.RWMutex{}
    count := 0
    for _, obj := range objKeys {
        wg.Add(1)
        go func(obj string) {
            defer wg.Done()
            if re.Match([]byte(obj)) {
                dirCh <-obj
            } else {
                fileCh <-obj
            }
        }(obj)
    }
    go func() {
        for {
            select {
            case dir, ok := <-dirCh:
                if ok {
                    rwmu.Lock()
                    dirLsit = append(dirLsit, dir)
                    rwmu.Unlock()
                } else {
                    rwmu.Lock()
                    count += 1
                    rwmu.Unlock()
                    break
                }
            default:
            }
        }
    }()
    go func() {
        for {
            select {
            case file, ok := <-fileCh:
                if ok {
                    rwmu.Lock()
                    dirLsit = append(fileList, file)
                    rwmu.Unlock()
                } else {
                    rwmu.Lock()
                    count += 1
                    rwmu.Unlock()
                    break
                }
            default:
            }
        }
    }()
    go func() {
        for {
            if count == 2 {
                done <-true
                break
            }
        }
    }()
    wg.Wait()
    close(dirCh)
    close(fileCh)
    <-done
    close(done)
    return dirLsit, fileList, nil
}

type DownloadObjectConfig struct {
    Bucket    string
    LocalDir  string
    Buffer    bool
    Force     bool
}

func (sc *S3Client) DownloadObjects(ctx context.Context, conf *DownloadObjectConfig, objKey ...string) error {
    /* 
        You can send 3,500 PUT/COPY/POST/DELETE or 5,500 GET/HEAD requests per second per prefix in an Amazon S3 bucket.
    */
    downloader := s3manager.NewDownloader(sc.Client, func(d *s3manager.Downloader) {
        d.PartSize = 64 * 1024 * 1024 // 64MB per part
    })
    dir := strings.TrimRight(conf.LocalDir, "/")
    args := make([]interface{}, 2)
    args[0] = downloader
    args[1] = &DownloadObjectConfig {
        Bucket:    conf.Bucket,
        LocalDir:  dir,
        Buffer:    conf.Buffer,
        Force:     conf.Force,
    }
    list := worker_pool.StringsToIfs(objKey)
    wp := worker_pool.NewWorkerPool(sc.CoDownloads)
    jobs := &worker_pool.QueuedJobProfile {
        Task:    sc.DownloadObjectFromBucket,
        Inputs:  list,
        Params:  args,
    }
    objList := make([]interface{}, 0)
    go wp.Allocate(ctx, jobs)
    go wp.Read(ctx, &objList)
    wp.Consume(ctx)
    wp.Wait(ctx)
    return nil
}

func (sc *S3Client) DownloadObjectFromBucket(ctx context.Context, data interface{}, params ...interface{}) (interface{}, error) {
    objKey := data.(string)
    objDir := filepath.Dir(objKey)
    downloader := params[0].(*s3manager.Downloader)
    conf := params[1].(*DownloadObjectConfig)
    if _, err := os.Stat(fmt.Sprintf("%s/%s", conf.LocalDir, objDir)); os.IsNotExist(err) {
        if err = os.MkdirAll(fmt.Sprintf("%s/%s", conf.LocalDir, objDir), 0755); err != nil {
            return nil, err
        }
    }
    file := fmt.Sprintf("%s/%s", conf.LocalDir, objKey)
    if _, err := os.Stat("/path/to/whatever"); err == nil && ! conf.Force {
        return nil, fmt.Errorf("The file %s already exists.\n", file)
    }
    localFile, _ := os.Create(file)
    defer localFile.Close()
    if conf.Buffer {
        objHead, err := sc.Client.HeadObject(ctx, &s3.HeadObjectInput {
            Bucket:  aws.String(conf.Bucket),
            Key:     aws.String(objKey),
        })
        if err != nil {
            return nil, err
        }
        buf := make([]byte, int(objHead.ContentLength))
        w := s3manager.NewWriteAtBuffer(buf)
        _, err = downloader.Download(ctx, w, &s3.GetObjectInput {
            Bucket:  aws.String(conf.Bucket),
            Key:     aws.String(objKey),
        })
        _, err = localFile.Write(buf)
    } else {
        _, err := downloader.Download(ctx, localFile, &s3.GetObjectInput {
            Bucket:  aws.String(conf.Bucket),
            Key:     aws.String(objKey),
        })
        if err != nil {
            return nil, err
        }
        log.Printf("[Downloaded]: %s\n", objKey)
    }
    return objKey, nil
}

type UploadObjectConfig struct {
    Bucket    string
    Folder    string
    Force     bool
}

func (sc *S3Client) UploadObjects(ctx context.Context, conf *UploadObjectConfig, localFiles ...string) error {
    uploader := s3manager.NewUploader(sc.Client, func(u *s3manager.Uploader) {
        // Define a strategy that will buffer 25 MiB in memory
        u.BufferProvider = s3manager.NewBufferedReadSeekerWriteToPool(25 * 1024 * 1024)
        u.PartSize = 64 * 1024 * 1024 // 64MB per part
    })
    dir := strings.TrimLeft(conf.Folder, "/")
    args := make([]interface{}, 2)
    args[0] = uploader
    args[1] = &UploadObjectConfig {
        Bucket:  conf.Bucket,
        Folder:  dir,
        Force:   conf.Force,
    }
    list := worker_pool.StringsToIfs(localFiles)
    wp := worker_pool.NewWorkerPool(sc.CoUploads)
    jobs := &worker_pool.QueuedJobProfile {
        Task:    sc.UploadObjectToBucket,
        Inputs:  list,
        Params:  args,
    }
    fileList := make([]interface{}, 0)
    go wp.Allocate(ctx, jobs)
    go wp.Read(ctx, &fileList)
    wp.Consume(ctx)
    wp.Wait(ctx)
    return nil
    return nil
}

func (sc *S3Client) UploadObjectToBucket(ctx context.Context, data interface{}, params ...interface{}) (interface{}, error) {
    var (
        ae       smithy.APIError
        content  []byte
    )
    file := data.(string)
    if _, err := os.Stat(file); os.IsNotExist(err) {
        return nil, err
    }
    uploader := params[0].(*s3manager.Uploader)
    conf := params[1].(*UploadObjectConfig)
    objKey := fmt.Sprintf("%s/%s", conf.Folder, filepath.Base(file))

    _, err := sc.Client.HeadObject(ctx, &s3.HeadObjectInput {
        Bucket: aws.String(conf.Bucket),
        Key:    aws.String(objKey),
    })
    if errors.As(err, &ae) {
        switch ae.ErrorCode() {
        case "NoSuchKey":
        default:
            if ! conf.Force {
                return nil, fmt.Errorf("The object %s already exists.\n", objKey)
            }
        }
    }

    f, err := os.Open(file)
    if err != nil {
        return nil, err
    }
    defer f.Close()
    reader := bufio.NewReader(f)
    if _, err = reader.Read(content); err != nil {
        return nil, err
    }
    _, err = uploader.Upload(ctx, &s3.PutObjectInput {
        Bucket:  aws.String(conf.Bucket),
        Key:     aws.String(objKey),
        Body:    content,
    })
    if err != nil {
        return nil, err
    }
    return file, nil
}
