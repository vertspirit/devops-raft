package ssh_client

import (
    "fmt"
    "io"
    "io/ioutil"
    "log"
    "os"
    "path"
    "path/filepath"
    "strings"

    "github.com/pkg/sftp"
    "golang.org/x/crypto/ssh"
)

type SftpClient struct {
    Config        *SSHClient
    Client        *sftp.Client
    EnabledProxy  bool
}

func (sc *SftpClient) SetupClient() error {
    var (
        conf    *ssh.ClientConfig
        conn    *ssh.Client
        err     error
    )
    conf = sc.Config.SetupClientConfig()
    if sc.EnabledProxy {
        proxyConf := sc.Config.SetupBastionClientConfig()
        conn = sc.Config.CreateSSHProxyClient(proxyConf, conf)
        if conn == nil {
            return fmt.Errorf("Fail to create ssh proxy client.\n")
        }
    } else {
        conn = sc.Config.CreateSSHClient(conf)
        if conn == nil {
            return fmt.Errorf("Fail to create ssh client.\n")
        }
    }
    sc.Client, err = sftp.NewClient(conn)
    if err != nil {
        log.Printf("New SFTP Client Error: %v\n", err)
        return err
    }
    return nil
}

func (sc *SftpClient) CloseClient() {
    sc.Client.Close()
}

func (sc *SftpClient) UploadFiles(remoteDir string, files ...string) error {
    for _, f := range files {
        data, err := os.Open(f)
        if err != nil {
            log.Printf("Open File %s Error: %v\n", f, err)
            return err
        }
        defer data.Close()

        filePath := path.Join(remoteDir, path.Base(f))
        dst, err := sc.Client.Create(filePath)
        defer dst.Close()

        buf := make([]byte, 1024)
        for {
            n, err := data.Read(buf)
            if n == 0 {
                break
            }
            if err != nil {
                if err == io.EOF {
                    break
                } else {
                    log.Printf("Read Buffer Error: %v\n", err)
                    return err
                }
            }
            dst.Write(buf[:n]) // can't input buffer directly, or it will pad to fit 1024 bytes
            log.Printf("Upload %s file to remote server successfully.\n", f)
        }
    }
    return nil
}

func (sc *SftpClient) DownloadFiles(localDir string, files ...string) error {
    for _, f := range files {
        srcFile, err := sc.Client.Open(f)
        if err != nil {
            log.Printf("Open Remote File Error: %v\n", err)
            return err
        }
        defer srcFile.Close()

        dstFile, err := os.Create(path.Join(localDir, path.Base(f)))
        if err != nil {
            log.Printf("Create Local File Error: %v\n", err)
            return err
        }
        defer dstFile.Close()

        _, err = srcFile.WriteTo(dstFile)
        if err != nil {
            log.Printf("Write File Error: %v\n", err)
            return err
        }
        log.Printf("Download %s file from remote server successfully.\n", f)
    }
    return nil
}

func (sc *SftpClient) UploadDirectory(localDir, remoteDir string) error {
    dirs, err := ioutil.ReadDir(localDir)
    if err != nil {
        log.Printf("Read Diretory Error: %v\n", err)
        return err
    }
    for _, d := range dirs {
        localFile := path.Join(localDir, d.Name())
        remoteFile := path.Join(remoteDir, d.Name())
        if d.IsDir() {
            sc.Client.Mkdir(remoteFile)
            if err = sc.UploadFiles(remoteFile, localFile); err != nil {
                return err
            }
        } else {
            if err = sc.UploadFiles(remoteDir, localFile); err != nil {
                return err
            }
        }
        log.Printf("The directory or file %s has been uploaded to remote server successfully.\n", localFile)
    }
    return nil
}

func (sc *SftpClient) DownloadDirectory(remoteDir, localDir string) error {
    var (
        dList []string
        fList []string
    )
    walker := sc.Client.Walk(remoteDir)
    for walker.Step() {
        if walker.Err() != nil {
            continue
        }
        if walker.Stat().IsDir() {
            dList = append(dList, walker.Path())
        } else {
            fList = append(fList, walker.Path())
        }
    }
    err := os.MkdirAll(localDir, 0755)
    if err != nil {
        log.Printf("Create %s Directory Error: %v\n", localDir, err)
        return err
    }
    for _, dir := range dList {
        local := strings.Replace(dir, remoteDir, localDir, 1)
        if err = os.MkdirAll(local, 0755); err != nil {
            log.Printf("Create %s Directory Error: %v\n", dir, err)
            return err
        }
    }
    for _, file := range fList {
        local := strings.Replace(file, remoteDir, localDir, 1)
        path := filepath.Dir(local)
        if err = sc.DownloadFiles(path, file); err != nil {
            return err
        }
    }
    return nil
}

// https://pkg.go.dev/github.com/pkg/sftp
// https://pkg.go.dev/golang.org/x/crypto/ssh