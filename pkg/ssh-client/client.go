package ssh_client

import (
    "bufio"
    "encoding/base64"
    "errors"
    "fmt"
    "io"
    "io/ioutil"
    "log"
    "net"
    "os"
    "path/filepath"
    "sync"

    "github.com/rhysd/abspath"
    "golang.org/x/crypto/ssh"
    "golang.org/x/crypto/ssh/knownhosts"
)

type SSHClient struct {
    RemoteHost       string  `yaml:"host"`
    RemotePort       int     `yaml:"port"`
    AuthType         string  `yaml:"auth_type"`
    User             string  `yaml:"username"`
    Password         string  `yaml:"password"`
    PrivateKey       string  `yaml:"privkey"`
    Insecure         bool    `yaml:"insecure"`
    AutoAddHostKey   bool    `yaml:"auto_add_host_key"`
    ProxyServer      string  `yaml:"proxy_server"`
    ProxyPort        int     `yaml:"proxy_port"`
    ProxyUser        string  `yaml:"proxy_user"`
    ProxyPassword    string  `yaml:"proxy_password"`
    ProxyAuthType    string  `yaml:"proxy_auth"`
    ProxyPrivateKey  string  `yaml:"proxy_privkey"`
    ProxyInsecure    bool    `yaml:"proxy_insecure"`
}

func (sc *SSHClient) SetupClientConfig() *ssh.ClientConfig {
    if sc.User == "" {
        sc.User = "root"
    }
    if sc.RemotePort == 0 {
        sc.RemotePort = 22
    }
    conf := &ssh.ClientConfig {
        User: sc.User,
    }
    if sc.Insecure {
        conf.HostKeyCallback = ssh.InsecureIgnoreHostKey()
    } else {
        var kherr *knownhosts.KeyError
        conf.HostKeyCallback = ssh.HostKeyCallback(func(host string, remote net.Addr, pubKey ssh.PublicKey) error {
            kh := ParseKnownHosts()
            err := kh(host, remote, pubKey)
            if errors.As(err, &kherr) && len(kherr.Want) > 0 {
                log.Printf(
                    "WARNING: %v is not a key of %s, either a MiTM attack or %s has reconfigured the host pub key.",
                    PubKeyToString(pubKey),
                    host,
                    host,
                )
                return kherr
            } else if errors.As(err, &kherr) && len(kherr.Want) == 0 {
                if sc.AutoAddHostKey {
                    log.Printf("WARNING: %s is not trusted.\n", host)
                    log.Printf(
                        "WARNING: Adding this key %q to known_hosts file.\n",
                        PubKeyToString(pubKey),
                    )
                    if err = AddHostKeyToKnownHosts(host, remote, pubKey); err != nil {
                        return err
                    }
                } else {
                    return fmt.Errorf("WARNING: %s is not trusted.\n", host)
                }
            }
            return nil
        })
    }
    switch sc.AuthType {
    case "password":
        conf.Auth = []ssh.AuthMethod{ssh.Password(sc.Password)}
    default:
        // default auth uses key to login
        if sc.PrivateKey == "" {
            log.Printf("Private key not found.")
        }
        keyfile, err := abspath.ExpandFrom(sc.PrivateKey)
        if err != nil {
            log.Printf("Fail to get the absolute path of private key file.")
        }
        key, err := ioutil.ReadFile(keyfile.String())
        if err != nil {
            log.Printf("Load private key file error: %v\n", err)
        }
        privkey, err := ssh.ParsePrivateKey(key)
        if err != nil {
            log.Printf("Private key pasrsing error: %v\n", err)
        }
        conf.Auth = []ssh.AuthMethod{ ssh.PublicKeys(privkey) }
    }
    return conf
}

func (sc *SSHClient) SetupBastionClientConfig() *ssh.ClientConfig {
    if sc.ProxyUser == "" {
        sc.ProxyUser = "root"
    }
    conf := &ssh.ClientConfig {
        User: sc.ProxyUser,
    }
    if sc.Insecure {
        conf.HostKeyCallback = ssh.InsecureIgnoreHostKey()
    } else {
        callback := ParseKnownHosts()
        conf.HostKeyCallback = callback
    }
    switch sc.ProxyAuthType {
    case "password":
        conf.Auth = []ssh.AuthMethod{ssh.Password(sc.ProxyPassword)}
    default:
        if sc.ProxyPrivateKey == "" {
            log.Fatalf("Private key not found.")
        }
        keyfile, err := abspath.ExpandFrom(sc.PrivateKey)
        if err != nil {
            log.Fatalf("Fail to get the absolute path of private key file.")
        }
        key, err := ioutil.ReadFile(keyfile.String())
        if err != nil {
            log.Fatalf("Load private key file error: %v\n", err)
        }
        privkey, err := ssh.ParsePrivateKey(key)
        if err != nil {
            log.Fatalf("Private key pasrsing error: %v\n", err)
        }
        conf.Auth = []ssh.AuthMethod{ ssh.PublicKeys(privkey) }
    }
    return conf
}

func (sc *SSHClient) CreateSSHClient(conf *ssh.ClientConfig) *ssh.Client {
    if sc.RemotePort == 0 {
        sc.RemotePort = 22
    }
    addr := fmt.Sprintf("%s:%d", sc.RemoteHost, sc.RemotePort)
    conn, err := ssh.Dial("tcp", addr, conf)
    if err != nil {
        log.Printf("The remote server connection by ssh error: %v\n", err)
        return nil
    }
    return conn
}

func (sc *SSHClient) CreateSSHProxyClient(bsconf, conf *ssh.ClientConfig) *ssh.Client {
    if sc.ProxyPort == 0 {
        sc.ProxyPort = 22
    }
    if sc.RemotePort == 0 {
        sc.RemotePort = 22
    }
    proxyAddr := fmt.Sprintf("%s:%d", sc.ProxyServer, sc.ProxyPort)
    connToProxy, err := ssh.Dial("tcp", proxyAddr, bsconf)
    if err != nil {
        log.Printf("The creation of ssh client for proxy server error: %v\n", err)
        return nil
    }
    proxyConn, err := connToProxy.Dial("tcp", proxyAddr)
    if err != nil {
        log.Printf("The proxy server connection error: %v\n", err)
        return nil
    }

    addr := fmt.Sprintf("%s:%d", sc.RemoteHost, sc.RemotePort)
    conn, chans, reqs, err := ssh.NewClientConn(proxyConn, addr, conf)
    if err != nil {
        log.Printf("The remote server connection by ssh error: %v\n", err)
        return nil
    }

    client := ssh.NewClient(conn, chans, reqs)
    return client
}

func (sc *SSHClient) CloseClient(client *ssh.Client) {
    client.Close()
}

func (sc *SSHClient) RunCommand(client *ssh.Client, cmd string) string {
    session, err := client.NewSession()
    if err != nil {
        log.Printf("New session from connection error: %v\n", err)
        return "new session from ssh connection error."
    }
    defer session.Close()
    defer client.Close()

    output, err := session.Output(cmd)
    if err != nil {
        log.Printf("Run remote command error: %v\n", err)
        return "run remote command error."
    }
    return string(output)
}

func (sc *SSHClient) RunCmdFollowing(client *ssh.Client, lines chan string, errch chan error, cmd string) error {
    session, err := client.NewSession()
    if err != nil {
        log.Printf("New Session Error: %v\n", err)
        return err
    }
    defer session.Close()
    defer client.Close()

    wg := sync.WaitGroup{}

    output, err := session.StdoutPipe()
    if err != nil {
        log.Printf("Setup Stdout Pipeline of Session Error: %v\n", err)
        return err
    }
    outerr, err := session.StderrPipe()
    if err != nil {
        log.Printf("Setup Stderr Pipeline of Session Error: %v\n", err)
        return err
    }

    wg.Add(1)
    go FollowingStream(output, wg, lines, errch)
    go FollowingStream(outerr, wg, lines, errch)

    if err := session.Start(cmd); err != nil {
        log.Printf("Run Command Error: %v\n", err)
        return err
    }

    wg.Add(1)
    go func() {
        if err := session.Wait(); err != nil {
            errch <- err
        }
    }()

    done := make(chan int)
    go func() {
        for {
            select {
            case ln := <-lines:
                fmt.Printf("%v\n", ln)
            case <-done:
                close(done)
                break
            }
        }
    }()

    go func() {
        wg.Wait()
        done <- 1
        close(errch)
        close(lines)
    }()

    return <-errch
}

func FollowingStream(rd io.Reader, wg sync.WaitGroup, lines chan<- string, errch chan<- error) {
    scanner := bufio.NewScanner(rd)
    scanner.Split(bufio.ScanLines)
    for scanner.Scan() {
        lines <- scanner.Text()
    }
    if err := scanner.Err(); err != nil {
        errch <- err
    }
}

func ParseKnownHosts() ssh.HostKeyCallback {
    knownHostsFile := filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts")

    file, err := os.OpenFile(knownHostsFile, os.O_CREATE, 0600)
    if err != nil {
        log.Printf("Create known_hosts Error: %v\n", err)
        return nil
    }
    file.Close()

    keyCallback, err := knownhosts.New(knownHostsFile)
    if err != nil {
        log.Printf("Create Host Key Callback Error: %v\n", err)
        return nil
    }
    return keyCallback
}

func AddHostKeyToKnownHosts(host string, remote net.Addr, pubKey ssh.PublicKey) error {
    filePath := filepath.Join(os.Getenv("HOME"), ".ssh", "known_hosts")
    file, err := os.OpenFile(filePath, os.O_APPEND | os.O_WRONLY, 0600)
    if err != nil {
        return err
    }
    defer file.Close()
    knownHosts := knownhosts.Normalize(remote.String())
    _, err = file.WriteString(knownhosts.Line([]string{ knownHosts }, pubKey))
    return err
}

func PubKeyToString(key ssh.PublicKey) string {
    return fmt.Sprintf("%s %s", key.Type(), base64.StdEncoding.EncodeToString(key.Marshal()))
}

func (sc *SSHClient) ScpFileFromRemote(client *ssh.Client, to, from string) error {
    session, err := client.NewSession()
    if err != nil {
        log.Printf("New Session Error: %v\n", err)
        return err
    }
    defer session.Close()
    defer client.Close()

    output, err := session.StdoutPipe()
    if err != nil {
        log.Printf("Setup Stdout Pipeline of Session Error: %v\n", err)
        return err
    }

    file, err := os.OpenFile(from, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
    if err != nil {
        log.Printf("Open File Error: %v\n", err)
        return err
    }
    defer file.Close()

    cmd := fmt.Sprintf("cat %s \n", to)
    if err := session.Start(cmd); err != nil {
        log.Printf("Run Command Error: %v\n", err)
        return err
    }
    _, err = io.Copy(file, output)
    if err != nil {
        return err
    }
    if err = session.Wait(); err != nil {
        return err
    }
    return nil
}

func (sc *SSHClient) ScpFileToRemote(client *ssh.Client, to, from string) error {
    session, err := client.NewSession()
    if err != nil {
        log.Printf("New Session Error: %v\n", err)
        return err
    }
    defer session.Close()
    defer client.Close()

    input, err := session.StdinPipe()
    if err != nil {
        log.Printf("Setup Stdin Pipeline of Session Error: %v\n", err)
        return err
    }
    defer input.Close()

    file, err := os.Open(from)
    if err != nil {
        log.Printf("Read File Error: %v\n", err)
        return err
    }
    w := bufio.NewReader(file)
    go io.Copy(input, w)
    if err = session.Wait(); err != nil {
        return err
    }
    return nil
}
