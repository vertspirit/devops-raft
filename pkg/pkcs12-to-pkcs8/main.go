package main

import (
    "crypto/x509"
    "encoding/pem"
    "errors"
    "flag"
    "github.com/youmark/pkcs8"
    "golang.org/x/crypto/pkcs12"
    "io/ioutil"
    "log"
    "os"
)

var (
    in       = flag.String("in", "", "pkcs#12 input file (private key and certificate only)")
    password = flag.String("password", "", "to unlock the pkcs#12 bundle")
    outkey   = flag.String("outkey", "", "output filename of private key in pkcs#8 PEM format")
    outcert  = flag.String("outcert", "", "output filename of certificate in PEM format")
)

func main() {
    flag.Parse()

    if *in == "" || *password == "" || *outkey == "" || *outcert == "" {
        flag.Usage()
        os.Exit(1)
    }

    data, err := ioutil.ReadFile(*in)
    if err != nil {
        log.Fatal(err)
    }

    privateKey, certificate, err := pkcs12.Decode(data, *password)
    if err != nil {
        log.Fatal(err)
    }

    if err := verify(certificate); err != nil {
        log.Fatal(err)
    }

    keyBytes, err := pkcs8.ConvertPrivateKeyToPKCS8(privateKey)
    if err != nil {
        log.Fatal(err)
    }

    //could write private key as binary DER encoded (instead of pem below)
    //_, err = ioutil.WriteFile(*outkey,keyBytes,0644)

    //write private key as pem
    keyFile, err := os.Create(*outkey)
    if err != nil {
        log.Fatal(err)
    }
    defer keyFile.Close()
    err = pem.Encode(keyFile, &pem.Block{Type: "PRIVATE KEY", Bytes: keyBytes})
    if err != nil {
        log.Fatal(err)
    }

    certFile, err := os.Create(*outcert)
    if err != nil {
        log.Fatal(err)
    }
    defer certFile.Close()
    err = pem.Encode(certFile, &pem.Block{Type: "CERTIFICATE", Bytes: certificate.Raw})
    if err != nil {
        log.Fatal(err)
    }
}

// verify checks if a certificate has expired
func verify(cert *x509.Certificate) error {
    _, err := cert.Verify(x509.VerifyOptions{})
    if err == nil {
        return nil
    }

    switch e := err.(type) {
    case x509.CertificateInvalidError:
        switch e.Reason {
        case x509.Expired:
            return ErrExpired
        default:
            return err
        }
    case x509.UnknownAuthorityError:
        // Apple cert isn't in the cert pool
        // ignoring this error
        return nil
    default:
        return err
    }
}

// Certificate errors
var (
    ErrExpired = errors.New("certificate has expired or is not yet valid")
)
