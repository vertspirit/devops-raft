package gcs

import (
    "net/http"
    "net"
    "crypto/tls"
    "time"
    "log"
    "context"
    "io"
    "cloud.google.com/go/storage"
    "google.golang.org/api/iterator"
    "google.golang.org/api/option"
    ghttp "google.golang.org/api/transport/http"
)

type GcsClient struct {
    Client        storage.Client
    BucketHandle  storage.BucketHandle
    Transport     http.RoundTripper
    Config        *GcsClientConfig
}

type GcsClientConfig struct {
    ProjectId        string         `yaml:"project_id"`
    Scope            string         `yaml:"scope"`
    KeyJson          string         `yaml:"key_json"`
    BucketName       string         `yaml:"bucket_name"`
    ChunkBufferSize  int            `yaml:"chunk_buffer_size"`
    Timeout          time.Duration  `yaml:"timeout"`
    EnableHttp2      bool           `yaml:"enable_http2"`
    Insecure         bool           `yaml:"insecure"`
}

func (gc *GcsClient) NewBucketHandle(ctx context.Context, opts ...option.ClientOption) error {
    if err := gc.SetupTransport(ctx); err != nil {
        log.Printf("Setup Transport Error: %v\n", err)
        return err
    }
    httpClient := gc.SetupHttpClient(ctx)
    opts = append(opts, option.WithHTTPClient(httpClient))
    gc.SetupClient(ctx, opts)
    gc.Client.Bucket(gc.BucketHandle(gc.Config.BucketName))
    return nil
}

func (gc *GcsClient) SetupClient(ctx context.Context, opts ...option.ClientOption) error {
    opts = append(opts, option.WithCredentialsFile(gc.Config.KeyJson))
    client, err := storage.NewClient(ctx, opts)
    if err != nil {
        log.Printf("Setup Storage Client Error: %v\n", err)
        return err
    }
    gc.Client = client
    return nil
}

func (gc *GcsClient) SetupTransport(ctx context.Context) error {
    var err error
    tsp := &http.Transport{
        Proxy: http.ProxyFromEnvironment,
        DialContext: (&net.Dialer {
            Timeout:   30 * time.Second,
            KeepAlive: 30 * time.Second,
        }).DialContext,
        ForceAttemptHTTP2:     true,
        MaxIdleConns:          200,
        MaxIdleConnsPerHost:   200,
        IdleConnTimeout:       90 * time.Second,
        TLSHandshakeTimeout:   10 * time.Second,
        ExpectContinueTimeout: 1 * time.Second,
    }
    if !gc.Config.EnableHttp2 {
        tsp.TLSNextProto = make(map[string]func(string, *tls.Conn) http.RoundTripper)
        tsp.ForceAttemptHTTP2 = false
    }
    if gc.Config.Insecure {
        tsp.TLSClientConfig = &tls.Config{
            InsecureSkipVerify: true,
        }
    }
    tspOpt := []option.ClientOption{option.WithScopes(gc.Config.Scope)}
    gc.Transport, err = ghttp.NewTransport(ctx, tsp, tspOpt...)
    if err != nil {
        log.Printf("Create Transport Error: %v\n", err)
        return err
    }
    return nil
}

func (gc *GcsClient) SetupHttpClient(ctx context.Context) *http.Client {
    client := &http.Client {
        Transport: gc.Transport,
        Timeout:   gc.Config.Timeout * time.Second,
    }
    return client
}

func (gc *GcsClient) Close() {
    gc.Client.Close()
}

func (gc *GcsClient) ListBuckets(ctx context.Context) ([]string, error) {
    var list []string
    it := gc.Client.Buckets(ctx, gc.Config.ProjectId)
    for {
        battrs, err := it.Next()
        if err == iterator.Done {
            break
        }
        if err != nil {
            return nil, err
        }
        list = append(list, battrs.Name)
    }
    return list, nil
}

func (gc *GcsClient) GetBucket(ctx context.Context, name string) (*storage.ObjectAttrs, error) {
    attrs, err := gc.Client.Bucket(name).Attrs(ctx)
    if err == storage.ErrBucketNotExist {
        log.Printf("The bucket %s does not exist.\n", name)
        return nil, err
    } else if err != nil {
        log.Printf("Get Bucket %s Error: %v\n", name, err)
        return nil, err
    }
    return attrs, nil
}

func (gc *GcsClient) ListObejcts(ctx context.Context, name, prefix, delimiter string) ([]string, error) {
    var list []string
    query := storage.Query {
        Delimiter: delimiter,
        Prefix:    prefix,
    }
    it := gc.BucketHandle.Objects(ctx, query)
    for {
        battrs, err := it.Next()
        if err == iterator.Done {
            break
        }
        if err != nil {
            return nil, err
        }
        list = append(list, battrs.Name)
    }
    return list, nil
}

func (gc *GcsClient) GetObejct(ctx context.Context, name string) (*storage.ObjectAttrs, error) {
    attrs, err := gc.BucketHandle.Object(name).Attrs(ctx)
    if err == storage.ErrObjectNotExist {
        log.Printf("The object %s does not exist.\n", name)
        return nil ,err
    } else if err != nil {
        log.Printf("Get Object %s Error: %v", name, err)
        return nil, err
    }
    return attrs, nil
}

func (gc *GcsClient) ObejctReader(ctx context.Context, name string) (int64, io.ReadCloser, error) {
    var cancel context.CancelFunc
    if gc.Config.Timeout == 0 {
        gc.Config.Timeout = 5
    }
    ctx, cancel = context.WithTimeout(ctx, gc.Config.Timeout)
    reader, err := gc.BucketHandle.Object(name).NewReader(ctx)
    if err != nil {
        log.Printf("New Reader For Object Error: %v\n", err)
        cancel()
        return 0, nil, err
    }
    return reader.Attrs.Size, reader, nil
}

func (gc *GcsClient) PutObejcts(ctx context.Context, data ...[]byte) error {
    return nil
}
