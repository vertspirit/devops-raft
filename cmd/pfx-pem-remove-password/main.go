package main

import (
    "flag"
    "log"
    ccert "devops-raft/pkg/certificates"
)

var (
    paramInput         string
    paramOutput        string
    paramFormat        string
    paramPassword      string
)

func init() {
    flag.StringVar(&paramInput, "input", "", "The input file.")
    flag.StringVar(&paramInput, "i", "", "The input file. (shorten)")
    flag.StringVar(&paramOutput, "output", "", "The output file.")
    flag.StringVar(&paramOutput, "o", "", "The output file. (shorten)")
    flag.StringVar(&paramFormat, "format", "", "The format of input and output file.")
    flag.StringVar(&paramFormat, "f", "", "The format of input and output file. (shorten)")
    flag.StringVar(&paramPassword, "password", "", "The password for input file.")
    flag.StringVar(&paramPassword, "p", "", "The password for input file. (shorten)")
    flag.Parse()

    if paramFormat != "pem" && paramFormat != "pfx" {
        log.Fatal("Only support pem or pfx certificates.")
    }
    if paramInput == "" || paramOutput == "" || paramFormat == "" || paramPassword == "" {
        log.Fatal("The -input, -output, -format or -password parameters are required.")
    }
}

func main() {
    var input interface{}
    if paramFormat == "pem" {
        input = &ccert.PemCertificateFile {
            CertFile: paramInput,
            Password: paramPassword,
        }
        input.(*ccert.PemCertificateFile).RemovePemPasword(paramOutput)
    }
    if paramFormat == "pfx" {
        input = &ccert.PfxCertificateFile {
            InputFile: paramInput,
            Password:  paramPassword,
        }
        input.(*ccert.PfxCertificateFile).RemovePfxPassword(paramOutput)
    }
}
