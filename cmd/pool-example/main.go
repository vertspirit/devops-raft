package main

import (
    "context"
    "flag"
    "fmt"
    "strings"
    "sync"
    "time"
    "github.com/goombaio/namegenerator"
    "devops-raft/pkg/worker-pool"
)

var (
    settings config
)

type config struct {
    total    int
    workers  int
    length   int
}

func createBunchNamesV1(ctx context.Context, num int) []string {
    ch := make(chan string, 1)
    wg := sync.WaitGroup{}

    seed := time.Now().UTC().UnixNano()
    nameGenerator := namegenerator.NewNameGenerator(seed)
    list := make([]string, 0)

    for n := 1; n <= num; n++ {
        wg.Add(1)
        go func() {
            defer wg.Done()
            name := nameGenerator.Generate()
            ch <- name
        }()
    }
    go func() {
        wg.Wait()
        close(ch)
    }()
    for name := range ch {
        list = append(list, name)
    }
    return list
}

func createBunchNamesV2(ctx context.Context, num int) []string {
    ch := make(chan string, 1)
    done := make(chan bool)
    wg := sync.WaitGroup{}
    rwmu := sync.RWMutex{}

    seed := time.Now().UTC().UnixNano()
    nameGenerator := namegenerator.NewNameGenerator(seed)
    list := make([]string, 0)

    for n := 1; n <= num; n++ {
        wg.Add(1)
        go func() {
            defer wg.Done()
            name := nameGenerator.Generate()
            ch <-name
        }()
    }
    go func() {
        for {
            select {
            case name, ok := <-ch:
                if ok {
                    rwmu.Lock()
                    list = append(list, name)
                    rwmu.Unlock()
                } else {
                    done <-true
                    break
                }
                
            default:
            }
        }
    }()
    wg.Wait()
    close(ch)
    <-done
    return list
}

func checkLength(ctx context.Context, data interface{}, args ...interface{}) (interface{}, error) {
    name := data.(string)
    length := args[0].(int)
    chars := strings.Count(name, "")
    if chars > length {
        return name, nil
    }
    return "", fmt.Errorf("Not macthed.\n")
}

func init() {
    settings = config{}
    flag.IntVar(&settings.total, "total", 10000, "The total names to create as jobs, default: 10000.")
    flag.IntVar(&settings.total, "t", 10000, "The total names to create as jobs, default: 10000. (shoten)")
    flag.IntVar(&settings.workers, "workers", 1000, "The number of worker to run jobs, default: 100.")
    flag.IntVar(&settings.workers, "w", 1000, "The number of worker to run jobs, default: 100. (shoten)")
    flag.IntVar(&settings.length, "length", 20, "The name length, default: 20.")
    flag.IntVar(&settings.length, "l", 20, "The name length, default: 20. (shoten)")
    flag.Parse()
}

func main() {
    start := time.Now()
    ctx := context.Background()
    nameList := createBunchNamesV1(ctx, settings.total)
    ifList := make([]interface{}, 0)

    count := 1
    fmt.Println("------START------")
    for _, name := range nameList {
        fmt.Printf("[%d]: %s\n", count, name)
        ifList = append(ifList, name)
        count ++
    }
    fmt.Println("------END------")

    wp := worker_pool.NewWorkerPool(settings.workers)
    arg := []interface{} { settings.length }
    jobProfile := &worker_pool.QueuedJobProfile {
        Task: checkLength,
        Inputs: ifList,
        Params: arg,
    }

    outputs := make([]interface{}, 0)
    go wp.Allocate(ctx, jobProfile)
    go wp.Read(ctx, &outputs)
    wp.Consume(ctx)
    wp.Wait(ctx)

    count = 1
    fmt.Println("------MATCHED------")
    out := worker_pool.IfsToStrings(outputs)
    for _, matched := range out {
        fmt.Printf("[%d]: %s\n", count, matched)
        count ++
    }
    fmt.Println("------MATCHED------")
    timeElapsed := time.Since(start)
    fmt.Printf("This took %s.\n", timeElapsed)
}
