package main

import (
    "flag"
    "fmt"

    certparse "devops-raft/pkg/certificates"
)

var (
    paramFile      string
    paramFormat    string
    paramPassword  string
)

func init() {
    flag.StringVar(&paramFile, "cert", "", "The path and filename of certificate.")
    flag.StringVar(&paramFile, "c", "", "The path and filename of certificate. (shorten)")
    flag.StringVar(&paramFormat, "format", "pem", "The certificate format to parse.")
    flag.StringVar(&paramFormat, "f", "pem", "The certificate format to parse. (shorten)")
    flag.StringVar(&paramPassword, "password", "", "The password for the certificate.")
    flag.StringVar(&paramPassword, "p", "", "The password for the certificate. (shorten)")
    flag.Parse()
}

func main() {
    crt := &certparse.Certificate {
        File:     paramFile,
        Format:   paramFormat,
        Password: paramPassword,
    }
    x509Cert := crt.LoadAndParseCertFile()
    nextDays := crt.GetExpiredNextDays(x509Cert)
    fmt.Printf("This certificate will be expired in the next %d days.\n", nextDays)
    if crt.IncludeCABundle(x509Cert) {
        fmt.Printf("This certificate includes bundle.\n")
    } else {
        if crt.VertifyPemCertChain(x509Cert) {
            fmt.Printf("This certificate has full chain.\n")
        } else {
            fmt.Printf("This certificate doesn't have full chain.\n")
        }
    }
}
