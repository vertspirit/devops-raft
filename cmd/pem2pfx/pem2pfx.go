package main

import (
    "flag"
    "log"
    ccert "devops-raft/pkg/certificates"
)

var (
    paramCrt           string
    paramKey           string
    paramCa            string
    paramOutput        string
    paramPassword      string
    paramPemPassword   string
)

func init() {
    flag.StringVar(&paramCrt, "crt", "", "The input certificate file with pem format to convert.")
    flag.StringVar(&paramCrt, "c", "", "The input certificate file with pem format to convert. (shorten)")
    flag.StringVar(&paramKey, "key", "", "The private key file with pem format to convert.")
    flag.StringVar(&paramKey, "k", "", "The private key file with pem format to convert. (shorten)")
    flag.StringVar(&paramCa, "ca", "", "The ca certificate file with pem format to convert.")
    flag.StringVar(&paramCa, "a", "", "The ca certificate file with pem format to convert. (shorten)")
    flag.StringVar(&paramPassword, "password", "", "The export password for pfx file.")
    flag.StringVar(&paramPassword, "p", "", "The export password for pfx file. (shorten)")
    flag.StringVar(&paramOutput, "pfx", "", "The pfx file to output.")
    flag.StringVar(&paramOutput, "f", "", "The pfx file to output. (shorten)")
    flag.StringVar(&paramPemPassword, "pem-password", "", "The password for encrypted pem file.")
    flag.StringVar(&paramPemPassword, "m", "", "The password for encrypted pem file. (shorten)")
    flag.Parse()

    if paramCrt == "" || paramKey == "" || paramOutput == "" {
        log.Fatal("No input file to convert or no output file to write.")
    }
}

func main() {
    pfxFile := &ccert.PemCertificateFile {
        CertFile:     paramCrt,
        PrivkeyFile:  paramKey,
        CaFile:       paramCa,
        OutputFile:   paramOutput,
        Password:     paramPassword,
        PemPassword:  paramPemPassword,
    }
    pfxFile.ConvertPemToPfx()
}
