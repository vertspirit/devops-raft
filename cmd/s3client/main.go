package main

import (
    "context"
    "flag"
    "fmt"
    "os"
    "devops-raft/pkg/aws-client"
)

type config struct {
    accessKeyId       string
    accessSeecretKey  string
    sessionToken      string
    region            string
    bucket            string
    credentialsFile   string
    operation         string
    localDir          string
    prefix            string
    delimiter         string
    before            int64
    coRoutines        int
    coDownloads       int
    coUploads         int
}

var (
    settings  *config
)

func init() {
    settings = &config{}
    flag.StringVar(&settings.accessKeyId, "key-id", "", "The aws access key id for authentication.")
    flag.StringVar(&settings.accessSeecretKey, "scret-key", "", "The aws access secret key for authentication.")
    flag.StringVar(&settings.sessionToken, "session-token", "", "The aws session token for authentication (optional).")
    flag.StringVar(&settings.bucket, "bucket", "", "The aws bucket to operate.")
    flag.StringVar(&settings.bucket, "b", "", "The aws bucket to operate. (shorten)")
    flag.StringVar(
        &settings.credentialsFile,
        "credentials-file",
        "",
        "The aws credentials file for authentication, and priority is lower than key-id and scret-key parameters. defualt: ~/.aws/credentials",
    )
    flag.StringVar(&settings.region, "region", "us-east-1", "The aws region to operate.")
    flag.StringVar(&settings.region, "r", "us-east-1", "The aws region to operate. (shorten)")
    flag.StringVar(&settings.operation, "operation", "list-buckets", "The operation to run.")
    flag.StringVar(&settings.operation, "o", "list-buckets", "The operation to run. (shorten)")
    flag.StringVar(&settings.localDir, "dir", "./", "The local directory to download or upload objects from.")
    flag.StringVar(&settings.localDir, "d", "./", "The local directory to download or upload objects from. (shorten)")
    flag.StringVar(&settings.prefix, "prefix", "", "Only the object key which start with specific prefix string.")
    flag.StringVar(&settings.delimiter, "delimiter", "", "Exclude the object contains specifc characters.")
    flag.Int64Var(&settings.before, "before", 0, "For the last modified objects before minutes ago.")
    flag.IntVar(&settings.coRoutines, "coroutines", 5000, "The maximum of coroutines to run tasks.")
    flag.IntVar(&settings.coDownloads, "downloads", 3500, "The maximum of downloads to run at the same time.")
    flag.IntVar(&settings.coUploads, "uploads", 2000, "The maximum of downloads to run at the same time.")
    flag.Parse()
}

func main() {
    ctx := context.Background()
    conf := &aws_client.AwsConfig{}
    cred, err := conf.CreateConfig(ctx)
    if err != nil {
        os.Exit(1)
    }
    client := &aws_client.S3Client {
        Config:       cred,
        CoRoutines:   settings.coRoutines,
        CoDownloads:  settings.coDownloads,
        CoUploads:    settings.coUploads,
    }
    if err = client.CreateClinent(ctx); err != nil {
        os.Exit(1)
    }
    switch settings.operation {
    case "list-buckets":
        list, err := client.ListBuckets(ctx, settings.region, true)
        if err != nil {
            os.Exit(1)
        }
        for id, bk := range list {
            fmt.Printf("[%d]: %s\n", id, bk)
        }
    case "list-objects":
        if &settings.bucket == nil || settings.bucket == "" {
            fmt.Println("You didn't give the bucket name to operate by '-bucket' or '-b' argument.")
            os.Exit(1)
        }
        listConf := &aws_client.ListObjectConfig {
            Bucket: settings.bucket,
        }
        if &settings.bucket != nil && settings.bucket != "" {
            listConf.Prefix = settings.prefix
        }
        if &settings.delimiter != nil && settings.delimiter != "" {
            listConf.Delimiter = settings.delimiter
        }
        list, err := client.ListObjects(ctx, listConf, settings.before)
        if err != nil {
            os.Exit(1)
        }
        for id, obj := range list {
            fmt.Printf("[%d]: %s\n", id, obj)
        }
    }
}
