package main

import (
    "flag"
    "log"
    ccert "devops-raft/pkg/certificates"
)

var (
    paramPfx           string
    paramOutKey        string
    paramOutCert       string
    paramPassword      string
    paramRmoveHeaders  bool
)

func init() {
    flag.StringVar(&paramPfx, "pfx", "", "The input pfx file to convert.")
    flag.StringVar(&paramPfx, "i", "", "The input pfx file to convert. (shorten)")
    flag.StringVar(&paramOutKey, "key", "", "The private key file to output.")
    flag.StringVar(&paramOutKey, "k", "", "The private key file to output. (shorten)")
    flag.StringVar(&paramOutCert, "crt", "", "The certificate file to output.")
    flag.StringVar(&paramOutCert, "c", "", "The certificate file to output. (shorten)")
    flag.StringVar(&paramPassword, "password", "", "The password for pfx file.")
    flag.StringVar(&paramPassword, "p", "", "The password for pfx file. (shorten)")
    flag.BoolVar(&paramRmoveHeaders, "remove-headers", false, "Rmove all optional headers.")
    flag.BoolVar(&paramRmoveHeaders, "h", false, "Rmove all optional headers. (shorten)")
    flag.Parse()

    if paramPfx == "" || paramOutKey == "" || paramOutCert == "" {
        log.Fatal("No input file to convert or no output file to write.")
    }
}

func main() {
    pfxFile := &ccert.PfxCertificateFile {
        InputFile:   paramPfx,
        PrivkeyFile: paramOutKey,
        CertFile:    paramOutCert,
        Password:    paramPassword,
        EraseHeaders:  paramRmoveHeaders,
    }
    pfxFile.ConvertPfxToPem()
}
