package main

import (
    "flag"
    "fmt"
    "log"
    "os"
    sc "devops-raft/pkg/ssh-client"
)

var (
    Version          string
    Build            string
    paramHost        string
    paramPort        int
    paramInsecure    bool
    paramAuthType    string
    paramUser        string
    paramPassword    string
    paramPrivateKey  string
    paramCommand     string
    paramFollow      bool
    paramAutoAddKey  bool
)

func init() {
    flag.StringVar(&paramHost, "host", "127.0.0.1", "The host to connect by ssh.")
    flag.StringVar(&paramHost, "h", "127.0.0.1", "The host to connect by ssh. (shorten)")
    flag.IntVar(&paramPort, "port", 22, "The port of ssh to connect.")
    flag.IntVar(&paramPort, "P", 22, "The port of ssh to connect. (shorten)")
    flag.StringVar(&paramAuthType, "auth", "key", "The way to login host by ssh.")
    flag.StringVar(&paramAuthType, "a", "key", "The way to login host by ssh. (shorten)")
    flag.StringVar(&paramUser, "user", "root", "The username to login host by ssh.")
    flag.StringVar(&paramUser, "u", "root", "The username to login host by ssh. (shorten)")
    flag.StringVar(&paramPassword, "password", "", "The password of username to login host by ssh.")
    flag.StringVar(&paramPassword, "p", "", "The password of username to login host by ssh. (shorten)")
    flag.StringVar(&paramPrivateKey, "key", "~/.ssh/id_rsa", "The private key to login host by ssh.")
    flag.StringVar(&paramPrivateKey, "k", "~/.ssh/id_rsa", "The private key to login host by ssh. (shorten)")
    flag.StringVar(&paramCommand, "command", "", "The command to run on the remote server.")
    flag.StringVar(&paramCommand, "c", "", "The command to run on the remote server. (shorten)")
    flag.BoolVar(&paramInsecure, "insecure", true, "Ignore the host key.")
    flag.BoolVar(&paramFollow, "follow", false, "Rum command in the following mode.")
    flag.BoolVar(&paramFollow, "f", false, "Rum command in the following mode.")
    flag.BoolVar(&paramAutoAddKey, "auto-add-key", true, "Enable that adds host key to known_hosts file automatically.")
    flag.Parse()

    if ! paramInsecure && paramPassword == "" {
        log.Fatalf("No password to login remote server by ssh.\n")
    }
}

func main() {
    config := &sc.SSHClient {
        RemoteHost:      paramHost,
        RemotePort:      paramPort,
        AuthType:        paramAuthType,
        User:            paramUser,
        Password:        paramPassword,
        PrivateKey:      paramPrivateKey,
        Insecure:        paramInsecure,
        AutoAddHostKey:  paramAutoAddKey,
    }
    conf := config.SetupClientConfig()
    client := config.CreateSSHClient(conf)
    if paramFollow {
        lines := make(chan string, 30)
        errch := make(chan error, 10)
        if err := config.RunCmdFollowing(client, lines, errch, paramCommand); err != nil {
            os.Exit(1)
        }
    } else {
        output := config.RunCommand(client, paramCommand)
        fmt.Printf("Output: %s\n", output)
    }
}
