package main

import (
    "flag"
    "fmt"
    "os"
    "devops-raft/pkg/ssh-client"
)

type args struct {
    host            string
    port            int
    auth            string
    username        string
    password        string
    privkey         string
    proxyHost       string
    proxyPort       int
    proxyUser       string
    proxyPassword   string
    proxyPrivkey    string
    proxyAuth       string
    action          string
    localDir        string
    remoteDir       string
    proxyInsecure   bool
    insecure        bool
    proxy           bool
    autoAddHostKey  bool
}

func ParseArgs() *args {
    settings := args{}
    flag.StringVar(&settings.host, "host", "127.0.0.1", "The remote ssh server.")
    flag.StringVar(&settings.host, "h", "127.0.0.1", "The remote ssh server. (shorten)")
    flag.IntVar(&settings.port, "port", 22, "The port of remote ssh server.")
    flag.IntVar(&settings.port, "p", 22, "The port of remote ssh server. (shorten)")
    flag.StringVar(&settings.auth, "auth", "key", "The auth type for ssh connection.")
    flag.StringVar(&settings.auth, "a", "key", "The auth type for ssh connection. (shorten)")
    flag.StringVar(&settings.username, "user", "root", "The username for ssh connection.")
    flag.StringVar(&settings.username, "u", "root", "The username for ssh connection. (shorten)")
    flag.StringVar(&settings.password, "password", "", "The password for ssh connection.")
    flag.StringVar(&settings.password, "w", "", "The password for ssh connection. (shorten)")
    flag.StringVar(&settings.privkey, "privkey", "~/.ssh/id_rsa", "The private key for ssh connection.")
    flag.StringVar(&settings.privkey, "k", "~/.ssh/id_rsa", "The private key for ssh connection. (shorten)")
    flag.StringVar(&settings.proxyHost, "proxy-host", "", "The ssh proxy server.")
    flag.IntVar(&settings.proxyPort, "proxy-port", 22, "The ssh proxy port.")
    flag.StringVar(&settings.proxyAuth, "proxy-auth", "key", "The auth type of ssh proxy.")
    flag.StringVar(&settings.proxyUser, "proxy-user", "", "The username for ssh proxy server.")
    flag.StringVar(&settings.proxyPassword, "proxy-password", "", "The password for ssh proxy server.")
    flag.StringVar(&settings.proxyPrivkey, "proxy-privkey", "~/.ssh/id_rsa", "The private key for ssh proxy server.")
    flag.BoolVar(&settings.proxyInsecure, "proxy-insecure", false, "Enable to skip host key check for ssh proxy.")
    flag.BoolVar(&settings.insecure, "insecure", false, "Enable to skip host key check.")
    flag.BoolVar(&settings.insecure, "i", false, "Enable to skip host key check. (shorten)")
    flag.BoolVar(&settings.proxy, "proxy", false, "Enable the proxy.")
    flag.BoolVar(&settings.proxy, "y", false, "Enable the proxy. (shorten)")
    flag.BoolVar(&settings.autoAddHostKey, "auto-add-key", true, "Enable that adds host key to known_hosts file automatically.")
    flag.StringVar(&settings.action, "action", "", "This could be 'download' or 'upload'.")
    flag.StringVar(&settings.localDir, "local-dir", "./", "The local directory or file to upload or download.")
    flag.StringVar(&settings.remoteDir, "remote-dir", "/tmp", "The remote directory or file to upload or download.")
    flag.Parse()
    return &settings
}

func main() {
    settings := ParseArgs()
    sshConfig := &ssh_client.SSHClient {
        RemoteHost:      settings.host,
        RemotePort:      settings.port,
        AuthType:        settings.auth,
        User:            settings.username,
        Password:        settings.password,
        PrivateKey:      settings.privkey,
        Insecure:        settings.insecure,
        AutoAddHostKey:  settings.autoAddHostKey,
    }
    sftpClient := &ssh_client.SftpClient {
        Config:        sshConfig,
        EnabledProxy:  settings.proxy,
    }
    if settings.proxy {
        sshConfig.ProxyServer     = settings.proxyHost
        sshConfig.ProxyPort       = settings.proxyPort
        sshConfig.ProxyUser       = settings.proxyUser
        sshConfig.ProxyPassword   = settings.proxyPassword
        sshConfig.ProxyAuthType   = settings.proxyAuth
        sshConfig.ProxyPrivateKey = settings.proxyPrivkey
        sshConfig.ProxyInsecure   = settings.proxyInsecure
    }
    if err := sftpClient.SetupClient(); err != nil {
        fmt.Printf("Setup SSH Client Error: %v\n", err)
        os.Exit(1)
    }
    defer sftpClient.CloseClient()
    if sftpClient.Client == nil {
        fmt.Println("Fail to connect to ssh server.")
        os.Exit(1)
    }
    switch settings.action {
    case "download":
        if err := sftpClient.DownloadDirectory(settings.remoteDir, settings.localDir); err != nil {
            fmt.Printf("Download from Remote Server Error: %v\n", err)
            os.Exit(1)
        }
    case "upload":
        info, err := os.Stat(settings.localDir)
        if err != nil {
            fmt.Printf("Get Informations Error: %v\n", err)
            os.Exit(1)
        }
        if info.IsDir() {
            if err = sftpClient.UploadDirectory(settings.localDir, settings.remoteDir); err != nil {
                fmt.Printf("Upload to Remote Server Error: %v\n", err)
                os.Exit(1)
            }
        } else {
            if err = sftpClient.UploadFiles(settings.remoteDir, settings.localDir); err != nil {
                fmt.Printf("Upload to Remote Server Error: %v\n", err)
                os.Exit(1)
            }
        }
    default:
        fmt.Printf("Unsupported action: %s\n", settings.action)
    }
}
