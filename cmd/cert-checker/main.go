package main

import (
    "flag"
    "fmt"
    "log"

    certcheck "devops-raft/pkg/certificates"
)

var (
    paramDomain    string
    paramPort      int
    paramCA        string
    paramInsecure  bool
    paramMinTLS    string
)

func init() {
    flag.StringVar(&paramDomain, "domain", "", "The website domain to check.")
    flag.StringVar(&paramDomain, "d", "", "The website domain to check. (shorten)")
    flag.IntVar(&paramPort, "port", 443, "The port of website to check.")
    flag.IntVar(&paramPort, "p", 443, "The port of website to check. (shorten)")
    flag.StringVar(&paramCA, "ca", "", "The CA certificate for a self-signed certificate.")
    flag.BoolVar(&paramInsecure, "insecure", false, "Enable the InsecureSkipVerify function.")
    flag.StringVar(&paramMinTLS, "tls", "1.2", "Accept the min version of TLS to verify. such as 1.1, 1.2 or 1.3")
    flag.Parse()

    if paramDomain == "" {
        log.Fatal("No domain to check!")
    }
}

func main() {
    site := &certcheck.WebSite{
        Domain:     paramDomain,
        Port:       paramPort,
        CAFile:     paramCA,
        Insecure:   paramInsecure,
        MinTLSVer:  paramMinTLS,
    }
    conn := site.Connect()
    defer conn.Close()

    if site.VerifyDomain(conn, paramDomain) {
        fmt.Println("Domain verified successfully.")
    } else {
        fmt.Println("Domain is not verified.")
    }

    nextDays := site.GetExpiredNextDays(conn)
    fmt.Printf("This certificate will be expired in next %d days.\n", nextDays)

    issuer := site.GetIssuer(conn)
    fmt.Printf("This certificate signed by: %s\n", issuer)

    if site.VerifyFullChain(conn) {
        fmt.Println("The certificate chain is ok.")
    } else {
        fmt.Println("The certificate chain is not complete.")
    }
}
