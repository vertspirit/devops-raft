package main

import (
    "fmt"
    "log"
    "time"
    "bytes"
    "encoding/json"
    "encoding/base64"
    "encoding/pem"
    "math/big"
    "io/ioutil"
    "strconv"
    "crypto"
    "crypto/rsa"
    "crypto/x509"
    "crypto/sha256"
    "crypto/rand"
    "crypto/tls"
    "net/http"
    "net/url"
)

func PrivateEncrypt(priv *rsa.PrivateKey, data []byte) (enc []byte, err error) {
    k := (priv.N.BitLen() + 7) / 8
    tLen := len(data)
    // rfc2313, section 8:
    // The length of the data D shall not be more than k-11 octets
    if tLen > k-11 {
        return nil, fmt.Errorf("The length of the data D shall not be more than k-11 octets.")
    }
    em := make([]byte, k)
    em[1] = 1
    for i := 2; i < k-tLen-1; i++ {
        em[i] = 0xff
    }
    copy(em[k-tLen:k], data)
    c := new(big.Int).SetBytes(em)
    if c.Cmp(priv.N) > 0 {
        return nil, fmt.Errorf("Encryption Error.")
    }
    var m *big.Int
    var ir *big.Int
    if priv.Precomputed.Dp == nil {
        m = new(big.Int).Exp(c, priv.D, priv.N)
    } else {
        // We have the precalculated values needed for the CRT.
        m = new(big.Int).Exp(c, priv.Precomputed.Dp, priv.Primes[0])
        m2 := new(big.Int).Exp(c, priv.Precomputed.Dq, priv.Primes[1])
        m.Sub(m, m2)
        if m.Sign() < 0 {
            m.Add(m, priv.Primes[0])
        }
        m.Mul(m, priv.Precomputed.Qinv)
        m.Mod(m, priv.Primes[0])
        m.Mul(m, priv.Primes[1])
        m.Add(m, m2)

        for i, values := range priv.Precomputed.CRTValues {
            prime := priv.Primes[2+i]
            m2.Exp(c, values.Exp, prime)
            m2.Sub(m2, m)
            m2.Mul(m2, values.Coeff)
            m2.Mod(m2, prime)
            if m2.Sign() < 0 {
                m2.Add(m2, prime)
            }
            m2.Mul(m2, values.R)
            m.Add(m, m2)
        }
    }

    if ir != nil {
        // Unblind.
        m.Mul(m, ir)
        m.Mod(m, priv.N)
    }
    enc = m.Bytes()
    return
}

// parse the private key from the pem format
func parseKey(key []byte) (*rsa.PrivateKey, error) {
    if block, _ := pem.Decode(key); block != nil {
        key = block.Bytes
    }
    parsedKey, err := x509.ParsePKCS8PrivateKey(key)
    if err != nil {
        parsedKey, err = x509.ParsePKCS1PrivateKey(key)
        if err != nil {
            return nil, err
        }
    }
    parsed, ok := parsedKey.(*rsa.PrivateKey)
    if !ok {
        return nil, fmt.Errorf("OAuth2: private key is invalid.")
    }
    return parsed, nil
}

type ServiceAccount struct {
    Scope           string
    SubClientEmail  string
    TokenURI        string  `json:"token_uri"`
    PorjectID       string  `json:"project_id"`
    ClientID        string  `json:"client_id"`
    ClientEmail     string  `json:"client_email"`
    Privkey         string  `json:"private_key"`
    PrivkeyID       string  `json:"private_key_id"`
    Timeout         int64
    InSecure        bool
}

type OAuth2Token struct {
    IdToken      string  `json:"id_token"`
    AccessToken  string  `json:"access_token"`
    TokenType    string  `json:"token_type"`
    ExpiresIn    int64   `json:"expires_in"`
}

func (sa *ServiceAccount) calculateSignature(exp int64) (string, error) {
    // epoch time for iat and exp
    iat := time.Now().Unix()
    iatStr := strconv.FormatInt(iat, 10)
    exp = time.Now().Add(time.Duration(exp) * time.Second).Unix()
    expStr := strconv.FormatInt(exp, 10)

    // header := fmt.Sprintf(`{"alg":"RS256","typ":"JWT","kid":"%s"}`, sa.PrivkeyID)
    header := `{"alg":"RS256","typ":"JWT"}`
    headerHash := base64.RawURLEncoding.EncodeToString([]byte(header))

    if sa.SubClientEmail == "" {
        sa.SubClientEmail = sa.ClientEmail
    }
    payload := map[string]string {
        "iss":    sa.ClientEmail,
        "sub":    sa.SubClientEmail,
        "scope":  sa.Scope,
        "aud":    sa.TokenURI,
        "iat":    iatStr,
        "exp":    expStr,
    }
    payloadString, _ := json.Marshal(payload)
    payloadHash := base64.RawURLEncoding.EncodeToString([]byte(payloadString))

    // get the pkscs private key
    key := []byte(sa.Privkey)
    if block, _ := pem.Decode(key); block != nil {
        key = block.Bytes
    }
    parsedKey, err := x509.ParsePKCS8PrivateKey(key)
    if err != nil {
        log.Printf("Parse PKCS8 Private Key Error: %v\n", err)
        parsedKey, err = x509.ParsePKCS1PrivateKey(key)
        if err != nil {
            log.Printf("Parse PKCS1 Private Key Error: %v\n", err)
            return "", err
        }
    }
    parsed, ok := parsedKey.(*rsa.PrivateKey)
    if !ok {
        log.Println("OAuth2: private key is invalid.")
        return "", fmt.Errorf("OAuth2: private key is invalid.")
    }

    // calculate signature bytes, crypto.SHA256 <-> crypto.Hash(0)
    signedData := fmt.Sprintf("%s.%s", headerHash, payloadHash)
    digest := sha256.Sum256([]byte(signedData))
    signature, err := rsa.SignPKCS1v15(rand.Reader, parsed, crypto.SHA256, digest[:])
    if err != nil {
        log.Printf("Signing Error: %s\n", err)
        return "", err
    }
    err = rsa.VerifyPKCS1v15(&parsed.PublicKey, crypto.SHA256, digest[:], signature)
    if err != nil {
        log.Printf("Verify PKCS1v15 Error: %s\n", err)
        return "", err
    }
    // join message with signature as JWT signature
    sigB64Url := base64.RawURLEncoding.EncodeToString(signature)
    jwtSig := fmt.Sprintf("%s.%s", signedData, sigB64Url)
    return jwtSig, nil
}

func (sa *ServiceAccount) receiveToken(exp int64) (int, *OAuth2Token, error) {
    var (
        client http.Client
        token  OAuth2Token
    )
    if sa.Timeout <= 0 {
        sa.Timeout = 15
    }
    config := &tls.Config {
        InsecureSkipVerify:  sa.InSecure,
    }
    trsp := &http.Transport {
        TLSClientConfig: config,
    }
    client = http.Client {
        Timeout:   time.Duration(sa.Timeout) * time.Second,
        Transport: trsp,
    }
    signature, err := sa.calculateSignature(exp)
    if err != nil {
        return http.StatusBadRequest, nil, err
    }
    payload := url.Values{}
    payload.Set("grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer")
    payload.Add("assertion", signature)
    req, err := http.NewRequest("POST", sa.TokenURI, bytes.NewBufferString(payload.Encode()))
    if err != nil {
       log.Printf("Unalbe to create the request for oauth2: %v\n", err)
       return http.StatusBadRequest, nil, err
    }
    req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
    resp, err := client.Do(req)
    if err != nil {
        log.Printf("Fail to request the oauth2 server: %v\n", err)
        return http.StatusBadRequest, nil, err
    }
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Printf("Unable to read reponse body: %v\n", err)
    }
    resp.Body.Close()
    if err := json.Unmarshal(body, &token); err != nil {
        log.Printf("Fail to parse response from oauth2 server: %v\n", err)
        return http.StatusBadRequest, nil, err
    }
    return http.StatusOK, &token, nil
}
