package main

import (
    "fmt"
    "log"
    "flag"
    "encoding/json"
    "os"
    "io/ioutil"
    "context"
)

var (
    serviceAccountFile  string
    tokenScopes         OAuthScopes
    expTime             int64
    timeout             int64
    enableGOAuth2       bool
)

type OAuthScopes struct {
    list []string
}

func (oas *OAuthScopes) String() string {
    var scopes string
    if len(oas.list) <= 0 {
        return "https://www.googleapis.com/auth/cloud-platform.readonly"
    }
    for _, scp := range oas.list {
        if scopes == "" {
            scopes = scp
        } else {
            scopes = scopes + " " + scp
        }
    }
    return scopes
}

func (oas *OAuthScopes) Set(val string) error {
    oas.list = append(oas.list, val)
    return nil
}

func init() {
    flag.StringVar(&serviceAccountFile, "file", "", "The json file of service account.")
    flag.StringVar(&serviceAccountFile, "f", "", "The json file of service account. (shorten)")
    flag.Var(&tokenScopes, "scope", "The scope of api token.")
    flag.Var(&tokenScopes, "s", "The scope of api token. (shorten)")
    flag.Int64Var(&expTime, "expiration", 3600, "The expiration time for api token, in seconds.")
    flag.Int64Var(&expTime, "e", 3600, "The expiration time for api token, in seconds. (shorten)")
    flag.Int64Var(&timeout, "timeout", 15, "The timeout for request google api, in seconds.")
    flag.Int64Var(&timeout, "t", 15, "The timeout for request google api, in seconds. (shorten)")
    flag.BoolVar(&enableGOAuth2, "google-oauth2", false, "Use the golang.org/x/oauth2/google to get access token.")
    flag.BoolVar(&enableGOAuth2, "g", false, "Use the golang.org/x/oauth2/google to get access token. (shorten)")
    flag.Parse()
}

func main() {
    if serviceAccountFile == "" {
        fmt.Println("The json file of service account is required.")
        os.Exit(1)
    }
    var jsonData ServiceAccount
    fileContent, err := ioutil.ReadFile(serviceAccountFile)
    if err != nil {
        log.Fatal(err)
    }
    err = json.Unmarshal([]byte(fileContent), &jsonData)
    if err != nil {
        log.Fatal(err)
    }
    // https://developers.google.com/identity/protocols/oauth2/scopes
    // full scope list for google api
    if tokenScopes.list == nil {
        jsonData.Scope = "https://www.googleapis.com/auth/cloud-platform"
        tokenScopes.list = []string{"https://www.googleapis.com/auth/cloud-platform"}
    } else {
        jsonData.Scope = tokenScopes.String()
    }

    jsonData.Timeout = timeout

    if enableGOAuth2 {
        fmt.Println("-----")
        ctx := context.TODO()
        ReceiveTokenByJWT(ctx, serviceAccountFile, tokenScopes.list...)
    } else {
        status, token, err := jsonData.receiveToken(expTime)
        if err != nil {
            log.Println("Get Token Error.")
            os.Exit(1)
        }
        fmt.Println("-----")
        fmt.Printf("Status Code:  %d\n", status)
        if token.AccessToken != "" {
            fmt.Printf("Access Token: %s\n", token.AccessToken)
            fmt.Printf("Token Type:   %s\n", token.TokenType)
            fmt.Printf("Expires In:   %d\n", token.ExpiresIn)
        }
        if token.IdToken != "" {
            // google returns id token if scope is of .readonly
            // such as https://www.googleapis.com/auth/cloud-platform.readonly
            fmt.Printf("ID Token:     %s\n", token.IdToken)
        }
    }
}
