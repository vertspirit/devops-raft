package main

import (
    "context"
    "io/ioutil"
    "log"
    "fmt"
    "golang.org/x/oauth2"
    "golang.org/x/oauth2/google"
)

func ReceiveTokenByJWT(ctx context.Context, keyJson string, scopes ...string) (*oauth2.Token , error) {
    data, err := ioutil.ReadFile(keyJson)
    if err != nil {
        log.Printf("Read Json File Error: %v\n", err)
        return nil, err
    }
    ts, err := google.JWTAccessTokenSourceWithScope(data, scopes...)
    if err != nil {
        log.Printf("Get JWT Access Token Error: %v\n", err)
        return nil, err
    }
    token, err := ts.Token()
    if err != nil {
        log.Printf("Extract Access Token Error: %v\n", err)
        return nil, err
    }
    fmt.Printf("Access Token:   %s\n", token.AccessToken)
    fmt.Printf("Token Type:     %s\n", token.TokenType)
    fmt.Printf("Refresh Token:  %s\n", token.RefreshToken)
    fmt.Printf("Expiry:         %v\n", token.Expiry)
    return token, nil
}

func ReceiveTokenByCredential(ctx context.Context, keyJson string, scopes ...string) (*oauth2.Token , error) {
    data, err := ioutil.ReadFile(keyJson)
    if err != nil {
        log.Printf("Read Json File Error: %v\n", err)
        return nil, err
    }
    creds, err := google.CredentialsFromJSON(ctx, data, scopes...)
    if err != nil {
        log.Printf("Get Access Token Error: %v\n", err)
        return nil, err
    }
    token, err := creds.TokenSource.Token()
    if err != nil {
        log.Printf("Extract Access Token Error: %v\n", err)
        return nil, err
    }
    fmt.Printf("Access Token:   %s\n", token.AccessToken)
    fmt.Printf("Token Type:     %s\n", token.TokenType)
    fmt.Printf("Refresh Token:  %s\n", token.RefreshToken)
    fmt.Printf("Expiry:         %v\n", token.Expiry)
    return token, nil
}
